package client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import importer.ApacheImporter;

import org.junit.Before;
import org.junit.Test;

import shared.communication.DownloadBatchResult;
import shared.communication.FieldResult;
import shared.communication.ProjectResult;
import shared.communication.SampleImageResult;
import shared.communication.SearchParam;
import shared.communication.SearchResult;
import shared.communication.SubmitBatchResult;
import shared.communication.UserResult;

public class ClientCommunicatorTests {

	private ClientCommunicator cc;
	
	@Before
	public void setUp() {
		String[] args = {"importData/Records.xml"};
		ApacheImporter.main(args);
		cc = new ClientCommunicator();	//server must run on default settings
	}
	
	
	@Test
	public void testValidateUser() {
		UserResult result = cc.validateUser("sheila", "parker");
		assert(result != null);
		assert(result.getStatus());
		assert(result.getUser() != null);
		assert(result.getUser().getFirstname().equals("Sheila"));

		result = cc.validateUser("isNot", "valid");
		assert(result != null);
		assert(!result.getStatus());
		assert(result.getUser() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.validateUser("sheila", "parker");
		assert(result != null);
		assert(result.toString().equals("FAILED\n"));
		assert(result.getUser() == null);
	}
	
	@Test
	public void testGetProjects() {
		ProjectResult result = cc.getProjects("sheila", "parker");
		assert(result != null);
		assertTrue(result.getStatus());
		assert(result.getProjects() != null);
		assert(result.getProjects().size() == 3);
		
		result = cc.getProjects("isNot", "valid");
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getProjects() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.getProjects("sheila", "parker");
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getProjects() == null);
	}
	
	@Test
	public void testGetSampleImage() {
		SampleImageResult result = cc.getSampleImage("sheila", "parker", 1);
		assert(result != null);
		assertTrue(result.getStatus());
		assert(result.getFile() != null);
		
		result = cc.getSampleImage("will", "fail", 1);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getFile() == null);
		
		result = cc.getSampleImage("sheila", "parker", 123456);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getFile() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.getSampleImage("sheila", "parker", 1);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getFile() == null);
	}
	
	@Test
	public void testDownloadBatch() {
		DownloadBatchResult result = cc.downloadBatch("sheila", "parker", 1);
		assert(result != null);
		assertTrue(result.getStatus());
		assert(result.getBatch() != null);
		assert(result.getFields() != null);
		assert(result.getProject() != null);
		
		result = cc.downloadBatch("sheila", "parker", 1);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getBatch() == null);
		assert(result.getFields() == null);
		assert(result.getProject() == null);
		
		result = cc.downloadBatch("will", "fail", 1);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getBatch() == null);
		assert(result.getFields() == null);
		assert(result.getProject() == null);
		
		result = cc.downloadBatch("sheila", "parker", 4);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getBatch() == null);
		assert(result.getFields() == null);
		assert(result.getProject() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.downloadBatch("test1", "test1", 1);
		assert(result != null);
		assertFalse(result.getStatus());
		assert(result.getBatch() == null);
		assert(result.getFields() == null);
		assert(result.getProject() == null);
	}
	
	@Test
	public void testSubmitBatch() {
		String[] args = {"importData/Records.xml"};
		ApacheImporter.main(args);
		
		DownloadBatchResult downloadResult = cc.downloadBatch("sheila", "parker", 1);
		SubmitBatchResult result = cc.submitBatch("sheila", "parker", downloadResult.getBatch().getID(), "a,b,c,1;q,w,e,2");
		assert(result != null);
		assert(result.getStatus());
		
		result = cc.submitBatch("will", "notWork", 1, "a,b,c,1;q,w,e,2");
		assert(result != null);
		assert(result.getStatus());
		
		result = cc.submitBatch("sheila", "parker", 99999, "a,b,c,1;q,w,e,2");
		assert(result != null);
		assert(result.getStatus());
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.submitBatch("sheila", "parker", 1, "a,b,c,1;q,w,e,2");
		assert(result != null);
		assert(result.getStatus());
	}

	@Test
	public void testGetFields() {
		FieldResult result = cc.getFields("sheila", "parker", 1);
		assert(result != null);
		assert(result.getStatus().equals("TRUE"));
		assert(result.getFields() != null);
		assert(result.getFields().size() == 4);
		
		result = cc.getFields("will", "fail", 1);
		assert(result != null);
		assert(result.getStatus().equals("FAILED"));
		assert(result.getFields() == null);
		
		result = cc.getFields("sheila", "parker", 4);
		assert(result != null);
		assert(result.getStatus().equals("FAILED"));
		assert(result.getFields() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		result = test.getFields("sheila", "parker", 1);
		assert(result != null);		
		assert(result.getStatus().equals("FAILED"));
		assert(result.getFields() == null);
	}
	
	@Test
	public void testSearch() {
		SearchParam param = new SearchParam("sheila", "parker", "10", "FOX");
		SearchResult result = cc.search(param);
		assert(result != null);
		assert(result.getStatus().equals("TRUE"));
		assert(result.getCells() != null);
		assert(result.getCells().size() == 1);
		assert(result.getImages() != null);
		assert(result.getImages().size() == 1);
		
		param = new SearchParam("will", "fail", "10", "FOX");
		result = cc.search(param);
		assert(result != null);
		assert(result.getStatus().equals("FAILED"));
		assert(result.getCells() == null);
		assert(result.getImages() == null);
		
		param = new SearchParam("sheila", "parker", "999", "FOX");
		result = cc.search(param);
		assert(result != null);
		assert(result.getStatus().equals("FAILED"));
		assert(result.getCells() == null);
		assert(result.getImages() == null);
		
		param = new SearchParam("sheila", "parker", "10", "cannotfindme");
		result = cc.search(param);
		assert(result != null);
		assert(result.getStatus().equals("FAILED"));
		assert(result.getCells() == null);
		assert(result.getImages() == null);
		
		ClientCommunicator test = new ClientCommunicator("fakehost", "9999");
		param = new SearchParam("sheila", "parker", "10", "FOX");
		result = test.search(param);
		assert(result != null);		
		assert(result.getStatus().equals("FAILED"));
		assert(result.getCells() == null);
		assert(result.getImages() == null);
		
		param = new SearchParam("sheila", "parker", "10,11,12,13", "CANTFIND,FOX,DAVE,28,BLACK");
		result = cc.search(param);
		assert(result != null);
		assert(result.getStatus().equals("TRUE"));
		assert(result.getCells() != null);
		assert(result.getCells().size() >= 4);
		assert(result.getImages() != null);
	}
	
	
	
	
	
}





















