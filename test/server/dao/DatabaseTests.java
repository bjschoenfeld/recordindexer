package server.dao;

import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;

public class DatabaseTests {

	private Database db;
	
	@Test
	public void testInitialize() {
		try {
			Database.initialize();
			assert(true);
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testConnection() {
		db = new Database();
		assert(db.getConnection() == null);
		try {
			Database.initialize();
			db.startTransaction();
			assert(db.getConnection() != null);
			db.endTransaction(true);
			assert(db.getConnection() == null);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testGetDAOs() {
		db = new Database();
		assert(db.getBatchDAO() != null);
		assert(db.getUserDAO() != null);
		assert(db.getCellDAO() != null);
		assert(db.getProjectDAO() != null);
		assert(db.getFieldDAO() != null);
	}
	
}
