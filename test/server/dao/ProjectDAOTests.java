package server.dao;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Batch;
import shared.model.Field;
import shared.model.Project;

public class ProjectDAOTests {

	private Database db;
	
	@Before
	public void setUp() {
		try {
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testCreateTable() {
		try {
			db.startTransaction();
			db.getProjectDAO().createTable();
			db.endTransaction(true);
			assert(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testAdd() {
		Project proj = new Project(1, "1900 Census", 50, 137, 75);
		
		Field field1 = new Field(1, 1, 1, "name", 94, 32, "help.html", "knowndata.html");
		Field field2 = new Field(2, 1, 2, "age", 126, 10, "help.html", "knowndata.html");
		Field field3 = new Field(3, 1, 3, "gender", 136, 12, "help.html", "knowndata.html");
		ArrayList<Field> fields = new ArrayList<Field>();
		fields.add(field1);
		fields.add(field2);
		fields.add(field3);
		proj.setFields(fields);
		
		Batch batch1 = new Batch();
		batch1.setID(1);
		batch1.setParentProjectID(1);
		batch1.setFile("mypic1.png");
		batch1.setAvailable();
		Batch batch2 = new Batch();
		batch2.setID(2);
		batch2.setParentProjectID(1);
		batch2.setFile("mypic2.png");
		batch2.setAvailable();
		Batch batch3 = new Batch();
		batch3.setID(3);
		batch3.setParentProjectID(1);
		batch3.setFile("mypic3.png");
		batch3.setAvailable();
		
		ArrayList<Batch> batches = new ArrayList<Batch>();
		batches.add(batch1);
		batches.add(batch2);
		batches.add(batch3);
		proj.setBatches(batches);
		
		try {
			db.startTransaction();
			db.getProjectDAO().createTable();
			db.getFieldDAO().createTable();
			db.getBatchDAO().createTable();
			db.getProjectDAO().add(proj);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			int pkey = db.getProjectDAO().add(proj);
			db.endTransaction(true);
			assert(pkey == 2);
		} catch (DatabaseException e) {
			assert(true);
		} finally {
			db.endTransaction(false);
		}
		
	}
	
	@Test
	public void testGetAllProjects() {
		try {
			db.startTransaction();
			db.getProjectDAO().createTable();
			db.getProjectDAO().getAllProjects();
			assert(false);
			db.endTransaction(false);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			db.getProjectDAO().createTable();
			Project proj1 = new Project(1, "1900 Census", 50, 137, 75);
			Project proj2 = new Project(2, "1920 Census", 75, 119, 65);
			Project proj3 = new Project(3, "1930 Census", 75, 148, 50);
			db.getProjectDAO().add(proj1);
			db.getProjectDAO().add(proj2);
			db.getProjectDAO().add(proj3);
			ArrayList<Project> projects = db.getProjectDAO().getAllProjects();
			assert(projects.size() == 3);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
}
