package server.dao;

import importer.ApacheImporter;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Batch;

public class BatchDAOTests {

	private Database db;
	
	@Before
	public void setUp() {
		try {
			String[] args = {"importData/Records.xml"};
			ApacheImporter.main(args);
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}

	@Test
	public void testCreateTable() {
		try {
			db.startTransaction();
			db.getBatchDAO().createTable();
			db.endTransaction(true);
			assert(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testGetBatch() {
		try {
			db.startTransaction();
			db.getBatchDAO().createTable();
			db.getBatchDAO().getBatch(1, true);
			assert(false);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
		
		Batch batch = new Batch();
		batch.setID(135);
		batch.setParentProjectID(10);
		batch.setFile("mypic.png");
		batch.setAvailable();
		try {
			db.startTransaction();
			db.getBatchDAO().add(batch);
			Batch result = db.getBatchDAO().getBatch(10, true);
			assert(result != null);
			assert(result.getStatus().equals("AVAILABLE"));
			assert(result.getFile().equals("mypic.png"));
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testSetStatus() {
		try {
			db.startTransaction();
			db.getBatchDAO().createTable();
			
			Batch batch = new Batch();
			batch.setParentProjectID(10);
			batch.setFile("mypic.png");
			batch.setAvailable();
			int pkey = db.getBatchDAO().add(batch);
			batch.setID(pkey);
			batch.setCheckedOut();
			db.getBatchDAO().setStatus(pkey, "CHECKEDOUT");
			db.getBatchDAO().getBatch(10, true);
			assert(false);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		}
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
}
