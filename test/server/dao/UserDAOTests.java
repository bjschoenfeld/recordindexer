package server.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.User;

public class UserDAOTests {

	private Database db;
	
	@Before
	public void setUp() {
		try {
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testCreateTable() {
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			db.endTransaction(true);
			assert(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testAdd() {
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			User user = new User("bjs123", "abcde", "brandon", "smith", "bjs123@byu.edu", 5, 101);
			db.getUserDAO().add(user);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			User user = new User("bjs123", "abcde", "brandon", "smith", "bjs123@byu.edu", 5, 101);
			db.getUserDAO().add(user);
			db.endTransaction(true);
			assert(false);
		} catch (DatabaseException e) {
			assert(true);
		} finally {
			db.endTransaction(false);
		}
	}

	@Test
	public void testValidateUser() {
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			User user = new User("bjs123", "abcde", "brandon", "smith", "bjs123@byu.edu", 5, 101);
			db.getUserDAO().add(user);
			User temp = db.getUserDAO().validateUser(user.getUsername(), user.getPassword());
			assert(temp != null);
			assertEquals(user.toSQLString(), temp.toSQLString());
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			User user = new User("bjs123", "abcde", "brandon", "smith", "bjs123@byu.edu", 5, 101);
			db.getUserDAO().add(user);
			db.getUserDAO().validateUser(user.getUsername(), "wrong");
			assert(false);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		}
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testIncreaseRecordsIndexed() {
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			User user = new User("bjs123", "abcde", "brandon", "smith", "bjs123@byu.edu", 5, 101);
			db.getUserDAO().add(user);
			db.getUserDAO().increaseRecordsIndexed(user.getUsername(), 7);
			User temp = db.getUserDAO().validateUser(user.getUsername(), user.getPassword());
			assert(temp != null);
			assert(temp.getIndexedRecords() == 12);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testSetCurrentBatch() {
		try {
			db.startTransaction();
			db.getUserDAO().createTable();
			User user = new User("&654$", "word", "bob", "joe", "spamme@test.byu.edu", 0, -1);
			db.getUserDAO().add(user);
			db.getUserDAO().validateUser(user.getUsername(), user.getPassword());
			db.getUserDAO().setCurrentBatch(user.getUsername(), 147);
			User temp = db.getUserDAO().validateUser(user.getUsername(), user.getPassword());
			assert(temp != null);
			assert(temp.getCurrentBatch() == 147);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		}
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
	}
	
}
