package server.dao;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Cell;

public class CellDAOTests {

	Database db;
	
	@Before
	public void setUp() {
		try {
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testCreateTable() {
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testAdd() {
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			Cell cell = new Cell(123, "brandon", 4, 35, 2);
			db.getCellDAO().add(cell);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			Cell cell = new Cell(123, "brandon", 4, 35, 2);
			db.getCellDAO().add(cell);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(true);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testSearchSingle() {
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			Cell cell = new Cell(123, "brandon", 4, 35, 2);
			int pkey = db.getCellDAO().add(cell);
			cell.setID(pkey);
			String[] fieldIDs = {"4"};
			String[] searchValues = {"brandon"};
			ArrayList<Cell> matches = db.getCellDAO().search(fieldIDs, searchValues);
			assert(matches != null);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			Cell cell = new Cell(123, "brandon", 4, 35, 2);
			db.getCellDAO().add(cell);
			String[] fieldIDs = {"5"};
			String[] searchValues = {"brandon"};
			db.getCellDAO().search(fieldIDs, searchValues);
			assert(false);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			Cell cell = new Cell(123, "brandon", 4, 35, 2);
			db.getCellDAO().add(cell);
			String[] fieldIDs = {"4"};
			String[] searchValues = {"bob"};
			db.getCellDAO().search(fieldIDs, searchValues);
			db.endTransaction(true);
			assert(false);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
	}
	/*
	@Test
	public void testSearchMultiple() {
		try {
			db.startTransaction();
			db.getCellDAO().createTable();
			
			Cell cell1 = new Cell(123, "bob", 1, 35, 2);
			Cell cell2 = new Cell(123, "smith", 2, 35, 2);
			Cell cell3 = new Cell(123, "M", 4, 3, 2);
			Cell cell4 = new Cell(123, "34", 4, 4, 2);
			Cell cell5 = new Cell(123, "jones", 2, 35, 2);
			Cell cell6 = new Cell(123, "32", 4, 4, 2);
			
			int pkey = db.getCellDAO().add(cell1);
			cell1.setID(pkey);
			pkey = db.getCellDAO().add(cell2);
			cell2.setID(pkey);
			pkey = db.getCellDAO().add(cell3);
			cell3.setID(pkey);
			pkey = db.getCellDAO().add(cell4);
			cell4.setID(pkey);
			pkey = db.getCellDAO().add(cell5);
			cell5.setID(pkey);
			pkey = db.getCellDAO().add(cell6);
			cell6.setID(pkey);

			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		finally {
			db.endTransaction(false);
		}
		
		String[] fieldIDs = {"1,2,3,4"};
		String[] searchValues = {"brandon"};
		ArrayList<Cell> matches = db.getCellDAO().search(fieldIDs, searchValues);
		assert(matches != null);
	}
	*/
}
