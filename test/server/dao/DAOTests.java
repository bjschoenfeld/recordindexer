package server.dao;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;

public class DAOTests {

	Database db;
	
	@Before
	public void setUp() {
		try {
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testCreateTable() {
		String fields = "SSN INTEGER PRIMARY KEY NOT NULL UNIQUE, " +
						"FIRSTNAME TEXT NOT NULL, " +
						"LASTNAME TEXT NOT NULL";
		try {
			db.startTransaction();
			DAO.createTable(db, "CITIZENS", fields);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testAdd() {
		String tableName = "CITIZENS";
		String tableFields = "SSN INTEGER PRIMARY KEY NOT NULL UNIQUE, " +
				"FIRSTNAME TEXT NOT NULL, " +
				"LASTNAME TEXT NOT NULL";
		String fieldLabels = "SSN, FIRSTNAME, LASTNAME";
		String row = "123456789, 'BOB', 'SMITH'";
		
		try {
			db.startTransaction();
			DAO.createTable(db, tableName, tableFields);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);	// should not reach here
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			DAO.add(db, tableName, fieldLabels, row, true);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);	// should not reach here
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			DAO.add(db, tableName, fieldLabels, row, true);
			db.endTransaction(true);
			assert(false);	// should not reach here
		} catch (DatabaseException e) {
			assert(true);
		} finally {
			db.endTransaction(false);
		}
	}
	
}
