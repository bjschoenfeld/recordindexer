package server.dao;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Field;

public class FieldDAOTests {

	private Database db;
	
	@Before
	public void setUp() {
		try {
			db = new Database();
			Database.initialize();
		} catch (DatabaseException e) {
			assert(false);
		}
	}
	
	@Test
	public void testCreateTable() {
		try {
			db.startTransaction();
			db.getFieldDAO().createTable();
			db.endTransaction(true);
			assert(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testAdd() {
		try {
			db.startTransaction();
			db.getFieldDAO().createTable();
			Field field = new Field(0, 2, 4, "name", 94, 32, "help.html", "knowndata.html");
			int pkey = db.getFieldDAO().add(field);
			assert(pkey == 1);
			db.endTransaction(true);
		} catch (DatabaseException e) {
			assert(false);
		} finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			Field field = new Field(0, 2, 4, "name", 94, 32, "help.html", "knowndata.html");
			int pkey = db.getFieldDAO().add(field);
			db.endTransaction(true);
			assert(pkey == 2);
		} catch (DatabaseException e) {
			assert(true);
		} finally {
			db.endTransaction(false);
		}
	}
	
	@Test
	public void testGetFields() {
		try {
			db.startTransaction();
			db.getFieldDAO().createTable();
			db.getFieldDAO().getFields(2);
			assert(false);	
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(true);
		}
		finally {
			db.endTransaction(false);
		}
		
		try {
			db.startTransaction();
			db.getFieldDAO().createTable();
			Field field1 = new Field(1, 2, 1, "name", 94, 32, "help.html", "knowndata.html");
			Field field2 = new Field(2, 2, 2, "age", 126, 10, "help.html", "knowndata.html");
			Field field3 = new Field(3, 3, 3, "gender", 136, 12, "help.html", "knowndata.html");	//different project
			db.getFieldDAO().add(field1);
			db.getFieldDAO().add(field2);
			db.getFieldDAO().add(field3);
			ArrayList<Field> fields = db.getFieldDAO().getFields(2);
			assert(fields.size() == 2);
			db.endTransaction(true);
		} 
		catch (DatabaseException e) {
			assert(false);
		} 
		catch (NotFoundException e) {
			assert(false);
		}
		finally {
			db.endTransaction(false);
		}
	}

}
