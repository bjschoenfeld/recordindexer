package servertester.controllers;

import java.util.*;

import shared.communication.*;
import client.*;
import servertester.views.*;

public class Controller implements IController {

	private IView _view;
	
	public Controller() {
		return;
	}
	
	public IView getView() {
		return _view;
	}
	
	public void setView(IView value) {
		_view = value;
	}
	
	// IController methods
	//
	
	@Override
	public void initialize() {
		getView().setHost("localhost");
		getView().setPort("39640");
		operationSelected();
	}

	@Override
	public void operationSelected() {
		ArrayList<String> paramNames = new ArrayList<String>();
		paramNames.add("User");
		paramNames.add("Password");
		
		switch (getView().getOperation()) {
		case VALIDATE_USER:
			break;
		case GET_PROJECTS:
			break;
		case GET_SAMPLE_IMAGE:
			paramNames.add("Project");
			break;
		case DOWNLOAD_BATCH:
			paramNames.add("Project");
			break;
		case GET_FIELDS:
			paramNames.add("Project");
			break;
		case SUBMIT_BATCH:
			paramNames.add("Batch");
			paramNames.add("Record Values");
			break;
		case SEARCH:
			paramNames.add("Fields");
			paramNames.add("Search Values");
			break;
		default:
			assert false;
			break;
		}
		
		getView().setRequest("");
		getView().setResponse("");
		getView().setParameterNames(paramNames.toArray(new String[paramNames.size()]));
	}

	@Override
	public void executeOperation() {
		switch (getView().getOperation()) {
		case VALIDATE_USER:
			validateUser();
			break;
		case GET_PROJECTS:
			getProjects();
			break;
		case GET_SAMPLE_IMAGE:
			getSampleImage();
			break;
		case DOWNLOAD_BATCH:
			downloadBatch();
			break;
		case GET_FIELDS:
			getFields();
			break;
		case SUBMIT_BATCH:
			submitBatch();
			break;
		case SEARCH:
			search();
			break;
		default:
			assert false;
			break;
		}
	}
	
	private void validateUser() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		UserParam userParam = new UserParam(username, password);
		getView().setRequest(userParam.toString());
		UserResult result = null;
		result = cc.validateUser(username, password);
		getView().setResponse(result.toString());
		
	}
	
	private void getProjects() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		
		UserParam userParam = new UserParam(username, password);
		getView().setRequest(userParam.toString());
		
		ProjectResult result = null;
		result = cc.getProjects(username, password);
		getView().setResponse(result.toString());
		
	}
	
	private void getSampleImage() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		int projectID = Integer.parseInt(params[2]);
		ProjectParam projParam = new ProjectParam(username, password, projectID);
		getView().setRequest(projParam.toString());
		SampleImageResult result = null;
		result = cc.getSampleImage(username, password, projectID);
		getView().setResponse(result.toString());
	}
	
	private void downloadBatch() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		int projectID = Integer.parseInt(params[2]);
		ProjectParam projParam = new ProjectParam(username, password, projectID);
		getView().setRequest(projParam.toString());
		DownloadBatchResult result = null;
		result = cc.downloadBatch(username, password, projectID);
		getView().setResponse(result.toString());
	}
	
	private void submitBatch() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		int batchID = Integer.parseInt(params[2]);
		String fieldValues = params[3];
		SubmitBatchParam subParam = new SubmitBatchParam(username, password, batchID, fieldValues);
		getView().setRequest(subParam.toString());
		SubmitBatchResult result = null;
		result = cc.submitBatch(username, password, batchID, fieldValues);
		getView().setResponse(result.toString());
	}
	
	private void getFields() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		int projectID = 0;
		if(!params[2].equals("")) {
			projectID = Integer.parseInt(params[2]);
		}
		
		ProjectParam projParam = new ProjectParam(username, password, projectID);
		getView().setRequest(projParam.toString());
		
		FieldResult result = null;
		result = cc.getFields(username, password, projectID);
		getView().setResponse(result.toString());
	}
	
	
	
	private void search() {
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), getView().getPort());
		String[] params = getView().getParameterValues();
		String username = params[0];
		String password = params[1];
		String fields = params[2];
		String searchValues = params[3];
		
		SearchParam searchParam = new SearchParam(username, password, fields, searchValues);
		getView().setRequest(searchParam.toString());
		
		SearchResult result = null;
		result = cc.search(searchParam);
		getView().setResponse(result.toString());
	}

}

