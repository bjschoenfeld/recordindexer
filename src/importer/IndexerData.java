package importer;

import java.util.ArrayList;

import org.w3c.dom.Element;

import shared.model.Project;
import shared.model.User;

public class IndexerData {

	private ArrayList<User> users;
	private ArrayList<Project> projects;

	public IndexerData(Element root) {
		users = new ArrayList<User>();
		projects = new ArrayList<Project>();
		ArrayList<Element> rootElements = ApacheImporter.getChildElements(root);
		ArrayList<Element> userElements = ApacheImporter.getChildElements(rootElements.get(0));
		for(Element userElement : userElements) {
			users.add(new User(userElement));
		}
		ArrayList<Element> projectElements =
				ApacheImporter.getChildElements(rootElements.get(1));
		for(Element projectElement : projectElements) {
			projects.add(new Project(projectElement));
		}
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public ArrayList<Project> getProjects() {
		return projects;
	}
}
