package importer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/*
 *	This is the import you need to use the Apache Commons IO library.
 *	Look at the code marked {(**APACHE**)}
 //*/
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Batch;
import shared.model.Cell;
import shared.model.Field;
import shared.model.Project;
import shared.model.User;

public class ApacheImporter
{
	
	public static void main(String args[]) {
		String file = args[0];
		try {
			ApacheImporter.run(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private IndexerData indexerData;
	
	public static void run(String xmlFile) throws Exception {
		ApacheImporter ai = new ApacheImporter();
		ai.importData(xmlFile);
	}
	
	private void importData(String xmlFilename) throws Exception
	{
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		File xmlFile = new File(xmlFilename);
		File dest = new File("Records");
		
		File emptydb = new File("database" + File.separator + "empty" + File.separator + "recordindexer.sqlite");
		File currentdb = new File("database" + File.separator + "recordindexer.sqlite");
		
		/*
		 * (**APACHE**)
		 */
		try
		{
			//	We make sure that the directory we are copying is not the the destination
			//	directory.  Otherwise, we delete the directories we are about to copy.
			if(!xmlFile.getParentFile().getCanonicalPath().equals(dest.getCanonicalPath()))
				FileUtils.deleteDirectory(dest);
				
			//	Copy the directories (recursively) from our source to our destination.
			FileUtils.copyDirectory(xmlFile.getParentFile(), dest);
			
			//	Overwrite my existing *.sqlite database with an empty one.  Now, my
			//	database is completelty empty and ready to load with data.
			FileUtils.copyFile(emptydb, currentdb);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		/*
		 * (**APACHE**)
		 */
		
		File parsefile = new File(dest.getPath() + File.separator + xmlFile.getName());
		Document doc = builder.parse(parsefile);
		doc.getDocumentElement().normalize();
		Element root = doc.getDocumentElement();
		indexerData = new IndexerData(root);
		writeToDatabase();
	}

	private void writeToDatabase() throws DatabaseException {
		Database.initialize();
		Database db = null;
		try {
			db = new Database();
			db.startTransaction();
			createTables(db);
			addUsers(db);
			addProjects(db);
			db.endTransaction(true);
		} 
		finally {
			db.endTransaction(false);
		}
	}
	
	private void createTables(Database db) throws DatabaseException {
		db.getUserDAO().createTable();
		db.getProjectDAO().createTable();
		db.getFieldDAO().createTable();
		db.getBatchDAO().createTable();
		db.getCellDAO().createTable();
	}
	
	private void addUsers(Database db) throws DatabaseException {
		ArrayList<User> users = indexerData.getUsers();
		for(User user: users) {
			db.getUserDAO().add(user);
		}
	}
	
	private void addProjects(Database db) throws DatabaseException {
		ArrayList<Project> projects = indexerData.getProjects();
		for(Project proj: projects) {
			int projectID = db.getProjectDAO().add(proj);
			proj.setID(projectID);
			addFields(db, proj);
			addBatches(db, proj);
		}
	}
	
	private void addFields(Database db, Project project) throws DatabaseException {
		ArrayList<Field> fields = project.getFields();
		for(Field field: fields) {
			field.setParentProjectID(project.getID());
			int primaryKey = db.getFieldDAO().add(field);
			field.setID(primaryKey);
		}
	}
	
	private void addBatches(Database db, Project project) throws DatabaseException {
		ArrayList<Batch> batches = project.getBatches();
		for(Batch batch: batches) {
			batch.setParentProjectID(project.getID());
			int primaryKey = db.getBatchDAO().add(batch);
			batch.setID(primaryKey);
			addCells(db, project, batch);
		}
	}
	
	private void addCells(Database db, Project project, Batch batch) throws DatabaseException {
		ArrayList<Cell> cells = batch.getCells();
		ArrayList<Field> fields = project.getFields();
		for(int i = 0; i < cells.size(); i++) {
			Cell currentCell = cells.get(i);
			currentCell.setParentBatchID(batch.getID());
			int parentFieldIndex = i % fields.size();
			int parentFieldID = fields.get(parentFieldIndex).getID();
			currentCell.setParentFieldID(parentFieldID);
			int primaryKey = db.getCellDAO().add(currentCell);
			currentCell.setID(primaryKey);
		}
	}
	
	public static ArrayList<Element> getChildElements(Element root) {
		ArrayList<Element> result = new ArrayList<Element>();
		NodeList children = root.getChildNodes();
		for(int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if(child.getNodeType() == Node.ELEMENT_NODE){
				result.add((Element)child);
			}
		}
		return result;
	}
	
	public static String getValue(Element element) {
		String result = "";
		Node child = element.getFirstChild();
		result = child.getNodeValue();
		return result;
	}
}