package client.state;

public class Cell {
	public int record;
	public int field;
	
	public Cell(int record, int field) {
		this.record = record;
		this.field = field;
	}
}
