package client.state;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import shared.model.Field;
import shared.model.Project;

/**
 * Grid class represents all the cells' locations in a downloaded batch image.
 * @author bjs123
 *
 */
public class Spreadsheet {
	
	private int rows;
	private int cols;
	
	private String[][] values;
	private ArrayList<ArrayList<Rectangle2D.Double>> grid;
	
	private Cell selCell;
	
	public Spreadsheet(Project project, ArrayList<Field> fields) {
		rows = project.getRecordsPerBatch();
		cols = fields.size();
		
		values = new String[rows][cols];
		
		grid = new ArrayList<ArrayList<Rectangle2D.Double>>();
		int y = project.getFirstYCoord();
		int h = project.getRecordHeight();
		for(int r = 0; r < project.getRecordsPerBatch(); r++) {
			ArrayList<Rectangle2D.Double> record = new ArrayList<Rectangle2D.Double>();
			for(Field f: fields) {
				int x = f.getxCoord();
				int w = f.getWidth();
				record.add(new Rectangle2D.Double(x, y, w, h));
			}
			y += h;
			grid.add(record);
		}
		
		selCell = new Cell(0,0);
	}
	
	public String[][] getValues() {
		return values;
	}
	
	public void setValue(String value,	Cell cell) {
		values[cell.record][cell.field] = value;
	}
	
	public Cell getSelectedCell() {
		return selCell;
	}
	
	public void setSelectedCell(Cell cell) {
		this.selCell = cell;
	}
	
	public Rectangle2D.Double getSelectedRectangle() {
		return grid.get(selCell.record).get(selCell.field);
	}
	
	public boolean clickImage(double clickXCoord, double clickYCoord) {
		for(int r = 0; r < rows; r++) {
			for(int c = 0; c < cols; c++) {
				Rectangle2D.Double current = grid.get(r).get(c);
				if(current.getX() <= clickXCoord && 
				   clickXCoord <= current.getX() + current.getWidth() &&
				   current.getY() <= clickYCoord &&
				   clickYCoord <= current.getY() + current.getHeight())
				{
					selCell = new Cell(r, c);
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for(int r = 0; r < rows; r++) {
			if(r > 0) {
				result.append(";");
			}
			for(int c = 0; c < cols; c++) {
				if(c > 0) {
					result.append(",");
				}
				result.append(values[r][c]);
			}
		}
		return result.toString();
	}

}

