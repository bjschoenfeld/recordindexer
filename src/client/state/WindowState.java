package client.state;

import java.awt.Rectangle;

public class WindowState {

	public double zoomLevel;
	public int imageXCoord;
	public int imageYCoord;
	public boolean highlightsOn;
	public boolean imageInverted;
	
	public Rectangle windowDimensions;
	public int horizontalDividerPosition;
	public int verticalDividerPosition;

	public WindowState() {
		this.setToDefault();
	}
	
	public void setToDefault() {
		zoomLevel = 1.0;
		imageXCoord = 0;
		imageYCoord = 0;
		highlightsOn = true;
		imageInverted = false;
		
		windowDimensions = new Rectangle(200, 200, 1000, 800);
		horizontalDividerPosition = 375;
		verticalDividerPosition = 400;
	}	
}
