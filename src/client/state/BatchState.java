package client.state;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D.Double;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import shared.communication.DownloadBatchResult;
import shared.communication.ProjectParam;
import shared.communication.UserParam;
import shared.model.Batch;
import shared.model.Field;
import shared.model.Project;
import shared.model.User;
import client.gui.dataentry.spell.SpellCorrector;
import client.gui.dataentry.spell.SpellCorrector.NoSimilarWordFoundException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class BatchState {
	
// save and load the state ========================================================================
	
	private static final String SAVED_STATES_FOLDER = "states";
	
	/**
	 * Writes the given BatchState to an xml file.
	 * @param state
	 * @throws FileNotFoundException
	 */
	public static void save(BatchState state) {
		File directory = new File(SAVED_STATES_FOLDER);
		if(!directory.exists()) {
			directory.mkdirs();
		}
		
		XStream xmlStream = new XStream(new DomDriver());
		File path = getPathToUserState(state.getUser());
		try {
			xmlStream.toXML(state, new FileOutputStream(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the given user's last BatchState. 
	 * If the user has not logged in before, returns a new BatchState for the user. 
	 * @param user
	 * @return
	 */
	public static BatchState load(User user) {
		XStream xmlStream = new XStream(new DomDriver());
		File path = getPathToUserState(user);
		BatchState state = null;		
		try {
			state =  (client.state.BatchState) xmlStream.fromXML(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			state = new BatchState(user);
		}
		return state;
	}
	
	private static File getPathToUserState(User user) {
		return new File(SAVED_STATES_FOLDER + File.separator + user.getUsername() + ".xml");
	}
	
// the state ======================================================================================	
	
	private static final String pathToKnownDataFiles = "knownData" + File.separator;
	private static final String knownDataFileType = ".txt";
	
	private User user;
	private Project project;
	private ArrayList<Field> fields;
	private Batch batch;
	private Spreadsheet spreadsheet;
	private boolean batchDownloaded;
	private WindowState windowState;
	transient private SpellCorrector[] spellCorrectors;
	transient private ArrayList<BatchStateListener> listeners;
	
	/**
	 * Used to set the default indexer GUI settings if the user has not logged in before.
	 * @param user
	 */
	private BatchState(User user) {
		this.init(user);
	}
	
	private void init(User user) {
		this.user = user;
		this.project = null;
		this.fields = null;
		this.batch = null;
		this.spreadsheet= null;
		this.batchDownloaded = false;
		this.windowState = new WindowState();
		this.listeners = new ArrayList<BatchStateListener>();
	}
	
	public void importBatch(DownloadBatchResult result) {
		if(result.getStatus()) {
			project = result.getProject();
			fields = result.getFields();
			batch = result.getBatch();
			spreadsheet = new Spreadsheet(project, fields);
			batchDownloaded = true;
			windowState = new WindowState();
			listeners = new ArrayList<BatchStateListener>();
			initSpellCorrectors();
		}
	}
	
	private void initSpellCorrectors() {
		spellCorrectors = new SpellCorrector[fields.size()];
		for(int f = 0; f < fields.size(); f++) {
			File localPath = new File(getPathToKnownData(fields.get(f).getID()));
			if(fields.get(f).getKnownData() != null) {
				try {
					FileUtils.copyURLToFile(new URL(fields.get(f).getKnownData()), localPath);
					spellCorrectors[f] = new SpellCorrector();
					spellCorrectors[f].useDictionary(getPathToKnownData(fields.get(f).getID()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				spellCorrectors[f] = null;
			}
		}
	}
	
	private String getPathToKnownData(int fieldID) {
		return pathToKnownDataFiles + fieldID + knownDataFileType;
	}
	
	public boolean isMispelled(int row, int col) {
		if(spellCorrectors == null) {
			initSpellCorrectors();
		}
		boolean result = false;
		if(this.batchDownloaded) {
			String word = spreadsheet.getValues()[row][col];
			if(word != null && !word.equals("") && spellCorrectors[col] != null) {
				result = spellCorrectors[col].isMispelled(word);
			}
		}
		return result;
	}
	
	public Set<String> suggestSimilarWords(int row, int col) {
		if(spellCorrectors == null) {
			initSpellCorrectors();
		}
		Set<String> result = null;
		if(this.batchDownloaded) {
			String word = spreadsheet.getValues()[row][col];
			if(word != null && !word.equals("") && spellCorrectors[col] != null) {
				try {
					result = spellCorrectors[col].suggestSimilarWords(word);
				} catch (NoSimilarWordFoundException e) {
					result = null;
				}
			}
		}
		return result;
	}
	
	public void submitBatch() {
		this.init(user);
	}
	
	public User getUser() {
		return user;
	}

	public Project getProject() {
		return project;
	}

	public ArrayList<Field> getFields() {
		return fields;
	}

	public Batch getBatch() {
		return batch;
	}
	
	public String[][] getValues() {
		return spreadsheet.getValues();
	}
	
	public boolean hasDownloadedBatch() {
		return batchDownloaded;
	}

	public WindowState getWindowState() {
		return windowState;
	}
	
	public ArrayList<BatchStateListener> getBatchStateListeners() {
		return listeners;
	}
	
	public void setZoom(double scale) {
		this.windowState.zoomLevel = scale;
	}
	
	public double getZoom() {
		return windowState.zoomLevel;
	}
	
	public void setHighlightsOn(boolean on) {
		this.windowState.highlightsOn = on;
	}
	
	public boolean getHighlightsOn() {
		return this.windowState.highlightsOn;
	}
	
	public void setImageInverted(boolean inverted) {
		this.windowState.imageInverted = inverted;
	}
	
	public boolean getImageInverted() {
		return this.windowState.imageInverted;
	}
	
	public int getImageXCoord() {
		return this.windowState.imageXCoord;
	}

	public void setImageXCoord(int imageXCoord) {
		this.windowState.imageXCoord = imageXCoord;
	}

	public int getImageYCoord() {
		return this.windowState.imageYCoord;
	}

	public void setImageYCoord(int imageYCoord) {
		this.windowState.imageYCoord = imageYCoord;
	}

	public Rectangle getWindowDimensions() {
		return this.windowState.windowDimensions;
	}

	public void setWindowDimensions(Rectangle rectangle) {
		this.windowState.windowDimensions = rectangle;
	}

	public int getHorizontalDividerPosition() {
		return this.windowState.horizontalDividerPosition;
	}

	public void setHorizontalDividerPosition(int horizontalDividerPosition) {
		this.windowState.horizontalDividerPosition = horizontalDividerPosition;
	}

	public int getVerticalDividerPosition() {
		return this.windowState.verticalDividerPosition;
	}

	public void setVerticalDividerPosition(int verticalDividerPosition) {
		this.windowState.verticalDividerPosition = verticalDividerPosition;
	}
	
// synchronization ===============================================================================
	
	public interface BatchStateListener {
		public void valueChanged(String value, Cell cell);
		public void selectedCellChanged();
	}
	
	public void addListener(BatchStateListener bsl) {
		if(listeners == null) {
			listeners = new ArrayList<BatchStateListener>();
		}
		listeners.add(bsl);
	}

	public void valueChanged(String value, Cell cell) {
		spreadsheet.setValue(value, cell);
		for (BatchStateListener l : listeners) {
			l.valueChanged(value, cell);
		}
	}
	
	public void selectedCellChanged(Cell cell) {
		if(cell.field == -1) {
			cell = new Cell(cell.record, 0);
		}
		spreadsheet.setSelectedCell(cell);
		for (BatchStateListener l : listeners) {
			l.selectedCellChanged();
		}
	}
	
	public Cell getSelectedCell() {
		return spreadsheet.getSelectedCell();
	}
	
	public Double getSelectedRectangle() {
		return spreadsheet.getSelectedRectangle();
	}

	public void clickImage(double clickXCoord, double clickYCoord) {
		if(spreadsheet.clickImage(clickXCoord, clickYCoord)) {
			for (BatchStateListener l : listeners) {
				l.selectedCellChanged();
			}
		}
	}
	
// utility ========================================================================================
	
	public UserParam getUserParam() {
		return new UserParam(user.getUsername(), user.getPassword());
	}
	
	public ProjectParam getProjectParam(int projectID) {
		return new ProjectParam(user.getUsername(), user.getPassword(), projectID);
	}
	
	public String getFormattedValues() {
		return spreadsheet.toString();
	}


}

