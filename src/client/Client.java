package client;

import java.awt.EventQueue;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import shared.communication.DownloadBatchResult;
import shared.communication.SampleImageResult;
import shared.communication.SubmitBatchResult;
import shared.model.Project;
import shared.model.User;
import client.gui.IndexerGUI;
import client.gui.LoginGUI;
import client.state.BatchState;

public class Client {

	public static void main(String[] args) {
		Client client = null;
		
		if(args.length == 2) {
			String serverHost = args[0];
			String serverPort = args[1];
			client = new Client(serverHost, serverPort);
		}
		else {
			client = new Client();
		}
		
		client.run();
	}
	
	private static final String DEFAULT_SERVER_HOST = "localhost";
	private static final String DEFAULT_SERVER_PORT = "8081";	
	
	private ClientCommunicator clientCom;
	private LoginGUI loginGUI;
	private IndexerGUI indexerGUI;
	private BatchState state;
	private String username;
	private String password;
	
	/**
	 * Creates a ClientCommunicator with the default server and port.
	 */
	public Client() {
		this(DEFAULT_SERVER_HOST, DEFAULT_SERVER_PORT);
	}
	
	/**
	 * Creates a ClientCommunicator with the given server and port.
	 * @param serverHost
	 * @param serverPort
	 */
	public Client(String serverHost, String serverPort) {
		clientCom = new ClientCommunicator(serverHost, serverPort);
	}
	
	/**
	 * Runs the record indexer program and starts the login GUI.
	 */
	public void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {		
				loginGUI = new LoginGUI(Client.this);
				loginGUI.setVisible(true);
				indexerGUI = null;
			}
		});
	}
	
	/**
	 * Closes the login GUI and sends the user to the indexer GUI.
	 * @param user A valid user from the server database.
	 */
	public void login(User user) {
		username = user.getUsername();
		password = user.getPassword();
		loginGUI.dispose();
		state = BatchState.load(user);
		indexerGUI = new IndexerGUI(this);
		indexerGUI.setVisible(true);
	}
	
	/**
	 * Closes the indexer GUI and returns the user to the login GUI.
	 */
	public void logout() {
		indexerGUI.dispose();
		indexerGUI = null;
		username = null;
		password = null;
		loginGUI = new LoginGUI(this);
		loginGUI.setVisible(true);
	}
	
	public User validateUser(String username, String password) {
		return clientCom.validateUser(username, password).getUser();
	}
	
	public ArrayList<Project> getProjects() {
		return clientCom.getProjects(username, password).getProjects();
	}
	
	public URL getSampleImage(int projectID) {
		SampleImageResult result = clientCom.getSampleImage(username, password, projectID);
		try {
			return new URL(result.getFile());
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	public boolean downloadBatch(int projectID) {
		DownloadBatchResult result = clientCom.downloadBatch(username, password, projectID);
		if(result.getStatus()) {
			state.importBatch(result);
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean submitBatch() {
		SubmitBatchResult result = clientCom.submitBatch(username, password, 
				state.getBatch().getID(), state.getFormattedValues());
		if(result.getStatus()) {
			state.submitBatch();
			return true;
		}
		else {
			return false;
		}
	}

	public BatchState getBatchState() {
		return state;
	}

	
}
