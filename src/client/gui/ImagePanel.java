package client.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import client.state.BatchState;
import client.state.BatchState.BatchStateListener;

@SuppressWarnings("serial")
public class ImagePanel extends JComponent implements BatchStateListener {
	
	private static Image NULL_IMAGE = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
	private static Color NULL_RECTANGLE_COLOR = new Color(0,0,0,0);
	private static Color DEFAULT_RECTANGLE_COLOR = new Color(0,255,0,127);
	
	private BatchState state;
	
	private int w_translateX;
	private int w_translateY;
	private double scale;
	
	private boolean dragging;
	private int w_dragStartX;
	private int w_dragStartY;
	private int w_dragStartTranslateX;
	private int w_dragStartTranslateY;
	private AffineTransform dragTransform;

	private Image image;
	private boolean imageInverted;
	private ArrayList<DrawingShape> shapes;
	
	private Color currentColor;
	
	public ImagePanel(BatchState state) {
		this.state = state;
		this.state.addListener(this);
		
		initFormat();
		initDrag();

		shapes = new ArrayList<DrawingShape>();
		
		if(state.hasDownloadedBatch()) {
			initShapes();
		}
	}
	
	private void initShapes() {
		this.addMouseListener(mouseAdapter);
		this.addMouseMotionListener(mouseAdapter);
		this.addMouseWheelListener(mouseAdapter);
		
		this.w_translateX = state.getImageXCoord();
		this.w_translateY = state.getImageYCoord();
		this.scale = this.state.getZoom();
		this.image = loadImage(state.getBatch().getFile());

		shapes.add(new DrawingImage(image, new Rectangle2D.Double(0, 0, image.getWidth(null), image.getHeight(null))));
		
		this.imageInverted = false;
		if(this.state.getImageInverted()) {
			this.invertImage();
		}
		
		Rectangle2D.Double rectangle = state.getSelectedRectangle();
		if(this.state.getHighlightsOn()) {
			currentColor = DEFAULT_RECTANGLE_COLOR;
		}
		else {
			currentColor = NULL_RECTANGLE_COLOR;
		}
		shapes.add(new DrawingRect(rectangle, currentColor));
		this.repaint();
	}
	
	private void initFormat() {
		this.setBackground(new Color(200, 200, 200));
		this.setMinimumSize(new Dimension(100, 100));
		this.setMaximumSize(new Dimension(1000, 1000));		
	}
	private void initDrag() {
		dragging = false;
		w_dragStartX = 0;
		w_dragStartY = 0;
		w_dragStartTranslateX = 0;
		w_dragStartTranslateY = 0;
		dragTransform = null;
	}
	
	private Image loadImage(String imageFile) {
		try {
			return ImageIO.read(new URL(imageFile));
		}
		catch (IOException e) {
			return NULL_IMAGE;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		
		g2.setColor(getBackground());
		g2.fillRect(0,  0, getWidth(), getHeight());
		
		g2.translate(this.getWidth() /	2.0, this.getHeight() / 2.0);
		g2.scale(scale, scale);
		g2.translate(-this.getWidth() / 2.0 + w_translateX, -this.getHeight() / 2.0 +	w_translateY);

		for (DrawingShape shape : shapes) {
			shape.draw(g2);
		}
	}
	
	private MouseAdapter mouseAdapter = new MouseAdapter() {

		@Override
		public void mousePressed(MouseEvent e) {
			int d_X = e.getX();
			int d_Y = e.getY();
			
			AffineTransform transform = new AffineTransform();
			transform.translate(ImagePanel.this.getWidth() /	2.0, ImagePanel.this.getHeight() / 2.0);
			transform.scale(scale, scale);
			transform.translate(-ImagePanel.this.getWidth() / 2.0 + w_translateX, -ImagePanel.this.getHeight() / 2.0 +	w_translateY);

			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try	{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex) {
				return;
			}
			int w_X = (int)w_Pt.getX();
			int w_Y = (int)w_Pt.getY();
			
			boolean hitShape = false;
			
			Graphics2D g2 = (Graphics2D)getGraphics();
			for (DrawingShape shape : shapes) {
				if (shape.contains(g2, w_X, w_Y)) {
					hitShape = true;
					break;
				}
			}
			
			if (hitShape) {
				dragging = true;		
				w_dragStartX = w_X;
				w_dragStartY = w_Y;		
				w_dragStartTranslateX = w_translateX;
				w_dragStartTranslateY = w_translateY;
				dragTransform = transform;
			}
		}

		@Override
		public void mouseDragged(MouseEvent e) {		
			if (dragging) {
				int d_X = e.getX();
				int d_Y = e.getY();
				
				Point2D d_Pt = new Point2D.Double(d_X, d_Y);
				Point2D w_Pt = new Point2D.Double();
				try
				{
					dragTransform.inverseTransform(d_Pt, w_Pt);
				}
				catch (NoninvertibleTransformException ex) {
					return;
				}
				int w_X = (int)w_Pt.getX();
				int w_Y = (int)w_Pt.getY();
				
				int w_deltaX = w_X - w_dragStartX;
				int w_deltaY = w_Y - w_dragStartY;
				
				w_translateX = w_dragStartTranslateX + w_deltaX;
				w_translateY = w_dragStartTranslateY + w_deltaY;
				
				repaint();
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			initDrag();
//			double relativeClickX = (e.getX() - (w_translateX * scale)) / scale;
//			double relativeClickY = (e.getY() - (w_translateY * scale)) / scale;
//			
//			transform.translate(ImagePanel.this.getWidth() /	2.0, ImagePanel.this.getHeight() / 2.0);
//			transform.scale(scale, scale);
//			transform.translate(-ImagePanel.this.getWidth() / 2.0 + w_translateX, -ImagePanel.this.getHeight() / 2.0 +	w_translateY);
			
			double relativeClickX = (e.getX() - getWidth()/2) / scale + getWidth()/2 - w_translateX;
			double relativeClickY = (e.getY() - getHeight()/2) / scale + getHeight()/2 - w_translateY;
			
			state.clickImage(relativeClickX, relativeClickY);
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			scale *= (10 - e.getWheelRotation()) / 10.0;
			repaint();
		}	
	};

	interface DrawingShape {
		boolean contains(Graphics2D g2, double x, double y);
		void draw(Graphics2D g2);
		Rectangle2D getBounds(Graphics2D g2);	
	}

	class DrawingRect implements DrawingShape {

		private Rectangle2D rect;
		private Color color;
		
		public DrawingRect(Rectangle2D rect, Color color) {
			this.rect = rect;
			this.color = color;
		}

		@Override
		public boolean contains(Graphics2D g2, double x, double y) {
			return rect.contains(x, y);
		}

		@Override
		public void draw(Graphics2D g2) {
			g2.setColor(color);
			g2.fill(rect);
		}
		
		@Override
		public Rectangle2D getBounds(Graphics2D g2) {
			return rect.getBounds2D();
		}
	}

	class DrawingImage implements DrawingShape {

		private Image image;
		private Rectangle2D rect;
		
		public DrawingImage(Image image, Rectangle2D rect) {
			this.image = image;
			this.rect = rect;
		}

		@Override
		public boolean contains(Graphics2D g2, double x, double y) {
			return rect.contains(x, y);
		}

		@Override
		public void draw(Graphics2D g2) {
			g2.drawImage(image, (int)rect.getMinX(), (int)rect.getMinY(), (int)rect.getMaxX(), (int)rect.getMaxY(),
							0, 0, image.getWidth(null), image.getHeight(null), null);
		}	
		
		@Override
		public Rectangle2D getBounds(Graphics2D g2) {
			return rect.getBounds2D();
		}
	}
	
	public void zoomIn() {
		scale *= 1.25;		
		this.repaint();
		state.setZoom(scale);
	}

	public void zoomOut() {
		scale *= .8;
		this.repaint();
		state.setZoom(scale);
	}

	public void invertImage() {
		RescaleOp op = new RescaleOp(-1.0f, 255f, null);
		image = op.filter((BufferedImage) image, null);
		imageInverted = !imageInverted;
		shapes.set(0, new DrawingImage(image, shapes.get(0).getBounds(null)));
		this.repaint();
	}

	public void toggleHighlights() {
		if(currentColor.equals(DEFAULT_RECTANGLE_COLOR)) {
			currentColor = NULL_RECTANGLE_COLOR;
		}
		else {
			currentColor = DEFAULT_RECTANGLE_COLOR;
		}	
		
		shapes.set(1, new DrawingRect(shapes.get(1).getBounds(null), currentColor));
		this.repaint();
	}

	@Override
	public void selectedCellChanged() {
		Rectangle2D.Double rectangle = state.getSelectedRectangle();
		shapes.set(1, new DrawingRect(rectangle, currentColor));
		repaint();
	}

	@Override
	public void valueChanged(String value, client.state.Cell cell) {
		return;
	}

	public void save() {
		if(state.hasDownloadedBatch()) {
			state.setImageXCoord(w_translateX);
			state.setImageYCoord(w_translateY);
			state.setZoom(scale);
			state.setHighlightsOn(currentColor.equals(DEFAULT_RECTANGLE_COLOR));
			state.setImageInverted(this.imageInverted);
		}
	}

}



