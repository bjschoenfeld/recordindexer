package client.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import shared.model.Field;
import client.state.BatchState;
import client.state.BatchState.BatchStateListener;
import client.state.Cell;

@SuppressWarnings("serial")
class HelpPane extends JTabbedPane {
	
	//private static BufferedImage NULL_IMAGE = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
	
	private BatchState state;

	public HelpPane(BatchState state) {
		this.state = state;
		
		this.setTabPlacement(JTabbedPane.TOP);		
		
		this.addTab("Field Help", new JScrollPane(new FieldHelpPanel(this.state)));
		this.addTab("Image Navigation", new JPanel());
	}
	
	class FieldHelpPanel extends JEditorPane implements BatchStateListener {

			private BatchState state;
		    private String[] images;
		    private int selectedImageIndex;

		    public FieldHelpPanel(BatchState state) {
		    	this.state = state;
		    	state.addListener(this);
		    	this.setContentType("text/html");
		    	this.setEditable(false);
		    	if(state.hasDownloadedBatch()) {
			    	ArrayList<Field> fields = state.getFields();
			    	images = new String[fields.size()];
			    	for(int f = 0; f < fields.size(); f++) {            
						images[f] = fields.get(f).getHelpHTML();
			    	}
			    	selectedImageIndex = state.getSelectedCell().field;
			    	this.setEditable(false);
			    	try {
						this.setPage(new URL(images[selectedImageIndex]));
					} catch (IOException e) {
						e.printStackTrace();
					}
		    	}
		    }

			@Override
			public void valueChanged(String value, Cell cell) {
				return;
			}

			@Override
			public void selectedCellChanged() {
				if(selectedImageIndex != state.getSelectedCell().field) {
					selectedImageIndex = state.getSelectedCell().field;
					try {
						this.setPage(images[selectedImageIndex]);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		
	}
}
