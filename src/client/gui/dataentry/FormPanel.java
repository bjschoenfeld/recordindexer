package client.gui.dataentry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import shared.model.Field;
import client.state.BatchState;
import client.state.BatchState.BatchStateListener;
import client.state.Cell;

@SuppressWarnings("serial")
public class FormPanel extends JPanel implements BatchStateListener {
	
	private static final Color ERROR_COLOR = new Color(255, 0, 0, 255);
	private static final Color NORMAL_COLOR = Color.WHITE;

	private BatchState state;
	
	private JScrollPane listPane;
	private FormModel listModel;
	private JList<String> list;
	
	
	private JScrollPane dataEntryPane; 
	private JPanel dataPanel;
	private JPanel[] dataRows;
	private JTextField[] dataFields;
	
	public FormPanel(BatchState state) {
		this.state = state;
		this.state.addListener(this);
		if(state.hasDownloadedBatch()) {
			initListPane();
			initDataEntryPane();		
			
			this.setLayout(new BorderLayout());
			this.add(listPane, BorderLayout.WEST);
			this.add(dataEntryPane, BorderLayout.CENTER);
		}
	}
	
	private void initListPane() {
		listModel = new FormModel(this.state);
		list = new JList<String>(listModel);
		list.addMouseListener(mouseAdapter);
		listPane = new JScrollPane(list);
		listPane.setPreferredSize(new Dimension(100,100));
	}
	
	private void initDataEntryPane() {
		ArrayList<Field> fields = state.getFields();
		int rows = fields.size();
		
		dataPanel = new JPanel();
		dataPanel.setLayout(new GridLayout(rows, 1));
		
		dataRows = new JPanel[rows];
		dataFields = new JTextField[rows];
		for(int r = 0; r < rows; r++) {
			JPanel currentRow = new JPanel();
			currentRow.setLayout(new FlowLayout(FlowLayout.RIGHT));
			currentRow.add(new JLabel(fields.get(r).getTitle() + ":"));
			
			FormTextField textField = new FormTextField(r);

			currentRow.add(textField);
			dataRows[r] = currentRow;
			dataFields[r] = textField;
			dataPanel.add(currentRow);
		}
		
		dataEntryPane = new JScrollPane(dataPanel);
	}
	
	private MouseAdapter mouseAdapter = new MouseAdapter() {

		@Override
		public void mousePressed(MouseEvent e) {
			onClick(e);
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			onClick(e);
		}
		
		private void onClick(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				int index = list.locationToIndex(e.getPoint());
				if (index >= 0 && index < listModel.getSize()) {
					Cell current = state.getSelectedCell();
					state.selectedCellChanged(new Cell(index, current.field));
				}
			}
		}
	};
	
	@Override
	public void valueChanged(String value, Cell cell) {
		if(cell.record == list.getSelectedIndex() &&
		   !dataFields[cell.field].getText().equals(value)) // prevents infinte looping
		{
			dataFields[cell.field].setText(value);
		}
		
	}

	@Override
	public void selectedCellChanged() {
		Cell selCell = state.getSelectedCell();
		for(int f = 0; f < dataFields.length; f++) {
			String text = state.getValues()[selCell.record][f];
			dataFields[f].setText(text);
		}
		list.setSelectedIndex(state.getSelectedCell().record);
		dataFields[selCell.field].requestFocus();
	}

	class FormTextField extends JTextField {
		
		private int index;
		private boolean error;
		
		public FormTextField(int index) {
			this.index = index;
			
			String text = state.getValues()[state.getSelectedCell().record][index];
			this.setText(text);
			this.setPreferredSize(new Dimension(150, 25));
			initListeners();
		}
		
		@Override
		public void setText(String text) {
			super.setText(text);
			if(list.getSelectedIndex() >= 0 && state.isMispelled(list.getSelectedIndex(), index)) {
				error = true;
				this.setBackground(ERROR_COLOR);
			}
			else {
				error = false;
				this.setBackground(NORMAL_COLOR);
			}
		}
		
		private void initListeners() {
			this.addFocusListener(new FocusListener() {
				
				@Override
				public void focusGained(FocusEvent arg0) {
					state.selectedCellChanged(new Cell(list.getSelectedIndex(), FormTextField.this.index));
				}
	
				@Override
				public void focusLost(FocusEvent arg0) {
					String text = dataFields[FormTextField.this.index].getText();
					state.valueChanged(text, new Cell(list.getSelectedIndex(), FormTextField.this.index));
				}
				
			});
			
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					if(e.isPopupTrigger() && error) {
						SuggestionPopup popup = new SuggestionPopup(list.getSelectedIndex(), index, state);
						popup.show(FormTextField.this, e.getX(), e.getY());
					}
				}
			});
		}
	}
}