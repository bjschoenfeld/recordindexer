package client.gui.dataentry.spell;

public class Dictionary implements ITrie {
	
	private Node root;
	private int wordCount;	// unique words
	private int nodeCount;	
	
	public Dictionary() {
		root = new Node('#');
		wordCount = 0;
		nodeCount = 1;
	}
	
	public void add(String word) {
		if(word.length() > 0) {
			root.add(word.toLowerCase(), 0, root);
		}
	}
	
	public Node find(String word) {
		word = word.toLowerCase();
		return root.find(word, 0);
	}

	public int getWordCount() {
		return this.wordCount;
	}
	
	public int getNodeCount() {
		return nodeCount;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		root.toString(sb, "");	// empty string is used as a temp for recursion
		return sb.toString();
	}
	
	@Override
	public int hashCode() {
		return this.wordCount * this.nodeCount;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = (o != null && o instanceof Dictionary);
		if (result) {
			Dictionary other = (Dictionary)o;
			result = this.root.equals(other.root);
		}
		return result;
	}
	
	public int getFrequency(String word) {
		return find(word).getValue();
	}
	
	public class Node implements INode {
		private static final char indexBase = 'a';
		private static final int spaceIndex = 26;
		private static final int hyphenIndex = 27;
		private char letter;
		private int count = 0;
		private Node[] children = new Node[28];
		
		public Node(char letter) {
			this.letter = letter;
		}
		
		public char getLetter() {
			return this.letter;
		}
		public int getValue() {
			return this.count;
		}
		public void add(String word, int position, Node root){
			if(position == word.length()) {
				this.count++;	// we reached a valid word
				if(this.count == 1) wordCount++; // this is the first time we reached the word
			}
			else {
				char l = word.charAt(position);
				int index = this.getCharIndex(l);
				if(this.children[index] == null) {
					this.children[index] = new Node(l);
					nodeCount++;
				}
				this.children[index].add(word, position+1, root);
			}
		}
		public void toString(StringBuilder sb, String temp) {
			for(int i = 0; i < this.children.length; i++) {
				Node child = this.children[i];
				if(child != null) {
					if(child.getValue() > 0) sb.append(temp + child.getLetter() + "\n");
					child.toString(sb, temp + child.getLetter());
				}
			}
		}
		
		public Node find(String word, int pos) {
			if(pos == word.length()) {
				if(this.count > 0) return this;
				else return null;
			}
			else {
				char l = word.charAt(pos);
				int index = this.getCharIndex(l);
				Node child = this.children[index];
				if(child != null) return child.find(word, pos+1);
				else return null;
			}			
		}
		
		public boolean equals (Node other) {
			boolean result = this.letter == other.getLetter() && this.count == other.getValue();
			int i = 0;
			while(result && i < this.children.length) {
				Node thisChild = this.children[i];
				Node otherChild = other.children[i];
				if(thisChild != null && otherChild != null) result = thisChild.equals(otherChild);
				else if(thisChild != null || otherChild != null) result = false;
				i++;
			}
			return result;
		}
		
		private int getCharIndex(char l) {
			int index = l - indexBase;
			if(l == ' ') {
				index = spaceIndex;
			}
			else if(l == '-') {
				index = hyphenIndex;
			}
			return index;
		}
	}
}
