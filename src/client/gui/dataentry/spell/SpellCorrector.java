package client.gui.dataentry.spell;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class SpellCorrector {
	
	private static final char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 
											'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
											's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	private Dictionary dict;
	private SortedSet<String> found;
	private SortedSet<String> notfound;	
	
	public SpellCorrector() {
		
	}
	
	public void useDictionary(String filename) throws FileNotFoundException {
		dict = new Dictionary();
		Scanner scanner = new Scanner(new BufferedReader(new FileReader(filename)));
		scanner.useDelimiter(",");
		while (scanner.hasNext()) {
			dict.add(scanner.next().trim());
		}
		scanner.close();
	}
	
	public boolean isMispelled(String word) {
		word = word.toLowerCase();
		if(dict.find(word) != null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public SortedSet<String> suggestSimilarWords(String inputWord) throws NoSimilarWordFoundException {
		found = new TreeSet<String>();
		notfound = new TreeSet<String>();
		if(dict.find(inputWord) != null) {
			return null;
		}
		else {
			findEditDistOne(inputWord);
			findEditDistTwo();
			if(found.isEmpty()) throw new NoSimilarWordFoundException();
		}
		return found;	
	}
	
	private void findEditDistOne(String inputWord) {
		StringBuilder word = new StringBuilder(inputWord);
		deletion(word);
		transposition(word);
		alteration(word);
		insertion(word);
	}
	
	private void findEditDistTwo() {
		SortedSet<String> copyOfNotFound = new TreeSet<String>();
		copyOfNotFound.addAll(notfound);
		Iterator<String> it = copyOfNotFound.iterator();
		while(it.hasNext()) {
			String temp = it.next();
			findEditDistOne(temp);
		}
	}
	
	private void deletion(StringBuilder word) {
		for(int i = 0; i < word.length(); i++) {
			String temp = word.substring(0, i) + word.substring(i+1);
			if(dict.find(temp) != null) found.add(temp);
			else notfound.add(temp);
		}
	}
	
	private String transpose(StringBuilder word, int pos) {	// swaps word[pos] with word[pos+1]
		return word.substring(0, pos) + word.charAt(pos+1) + word.charAt(pos) + word.substring(pos+2);
	}
	
	private void transposition(StringBuilder word) {
		for(int i = 0; i < word.length()-1; i++) {
			String temp = transpose(word, i);
			if(dict.find(temp) != null) found.add(temp);
			else notfound.add(temp);
		}
	}
	
	private void alteration(StringBuilder word) {
		for(int i = 0; i < word.length(); i++) {
			for(int j = 0; j < alphabet.length; j++) {
				String temp = word.substring(0, i) + alphabet[j] + word.substring(i+1);
				if(dict.find(temp) != null) found.add(temp);
				else notfound.add(temp);
			}
		}
	}
	
	private void insertion(StringBuilder word) {
		for(int i = 0; i <= word.length(); i++) {
			for(int j = 0; j < alphabet.length; j++) {
				String temp = word.substring(0, i) + alphabet[j] + word.substring(i);
				if(dict.find(temp) != null) found.add(temp);
				else notfound.add(temp);
			}
		}
	}
	
	@Override
	public boolean equals(Object o) {
		boolean result = (o != null && o instanceof SpellCorrector);
		if(result) {
			SpellCorrector other = (SpellCorrector)o;
			result = this.dict.equals(other.dict);
		}
		return result;
	}
	
	@SuppressWarnings("serial")
	public static class NoSimilarWordFoundException extends Exception {}
}




























