package client.gui.dataentry;

import javax.swing.table.AbstractTableModel;

import client.state.BatchState;
import client.state.Cell;

@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel {

	private BatchState state;
	
	private boolean valueAlreadyChanged;	// prevents infinite looping

	public TableModel(BatchState state) {
		this.state = state;
		valueAlreadyChanged = false;
	}

	@Override
	public int getColumnCount() {
		return state.getFields().size() + 1;
	}

	@Override
	public String getColumnName(int column) {
		if(column == 0) {
			return "Record Number";
		}
		else {
			return state.getFields().get(column-1).getTitle();
		}
	}

	@Override
	public int getRowCount() {
		return state.getProject().getRecordsPerBatch();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return column > 0;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object result = null;
		if(column == 0) {
			result = Integer.toString(row+1);
		}
		else if(0 <= row && row < getRowCount() && 1 <= column && column < getColumnCount()) {
			result = state.getValues()[row][column-1];
		}
		else {
			throw new IndexOutOfBoundsException();
		}
		return result;
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		if(!valueAlreadyChanged) {
			if (0 <= row && row < getRowCount() && 1 <= column && column < getColumnCount()) {
				valueAlreadyChanged = true;
				state.valueChanged((String)value, new Cell(row, column-1));
			} 
			else {
				throw new IndexOutOfBoundsException();
			}
		}
		else {
			valueAlreadyChanged = false;
		}
	}

}
