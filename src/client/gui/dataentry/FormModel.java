package client.gui.dataentry;

import javax.swing.AbstractListModel;

import client.state.BatchState;

@SuppressWarnings("serial")
public class FormModel extends AbstractListModel<String> {

	private BatchState state;
	
	public FormModel(BatchState state) {
		this.state = state;
	}
	
	@Override
	public String getElementAt(int index) {
		return Integer.toString(index + 1);
	}

	@Override
	public int getSize() {
		return state.getProject().getRecordsPerBatch();
	}

}
