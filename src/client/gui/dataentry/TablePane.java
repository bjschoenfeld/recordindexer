package client.gui.dataentry;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import client.state.BatchState;
import client.state.BatchState.BatchStateListener;
import client.state.Cell;

@SuppressWarnings("serial")
public class TablePane extends JScrollPane implements BatchStateListener {
	
	private BatchState state;

	private TableModel model;
	private JTable table;
	
	public TablePane(BatchState state) {
		this.state = state;
		this.state.addListener(this);
		if(state.hasDownloadedBatch()) {
			initTable();
		}
	}
	
	private void initTable() {
		model = new TableModel(state);
		table = new JTable(model);
		table.setRowHeight(25);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setCellSelectionEnabled(true);
		table.getTableHeader().setReorderingAllowed(false);
		
		TableColumnModel columnModel = table.getColumnModel();		
		for (int i = 0; i < model.getColumnCount(); ++i) {
			TableColumn column = columnModel.getColumn(i);
			column.setMinWidth(100);
			column.setMaxWidth(100);
		}		
		for (int i = 0; i < model.getColumnCount(); ++i) {
			TableColumn column = columnModel.getColumn(i);
			column.setCellRenderer(new CellRenderer(this.state));
		}
		selectedCellChanged();
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {					
				int row = table.rowAtPoint(new Point(0, e.getY()));
				int column = table.columnAtPoint(new Point(e.getX(), 0)) - 1;
				if (e.isPopupTrigger()  && state.isMispelled(row, column) && column >= 0)	{
					SuggestionPopup popup = new SuggestionPopup(row, column, state);
					popup.show(table, e.getX(), e.getY());
				}
			}				
		});
		this.setViewportView(table);
	}

	@Override
	public void valueChanged(String value, Cell cell) {
		model.setValueAt(value, cell.record, cell.field+1);
		table.repaint();
	}

	@Override
	public void selectedCellChanged() {
		Cell selCell = state.getSelectedCell();
	    table.changeSelection(selCell.record, selCell.field + 1, false, false);
	}
}

@SuppressWarnings("serial")
class CellRenderer extends JLabel implements TableCellRenderer {
	
	private static final Border BORDER = new LineBorder(new Color(0,0,0,255), 2);
	private static final Color SELECTED = new Color(0, 255, 0, 127);
	private static final Color ERROR = new Color(255, 0, 0, 255);
	private static final Color NORMAL = Color.WHITE;
	
	private BatchState state;
	
	public CellRenderer(BatchState state) {
		this.state = state;
		setOpaque(true);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
			boolean hasFocus, int row, int column) 
	{
		String text = (String)value;
		this.setText(text);
		final int r = row;
		final int c = column;
		Color cellColor = NORMAL;
		Border cellBorder = null;
		if(isSelected) {
			cellBorder = BORDER;
			cellColor = SELECTED;
			state.selectedCellChanged(new Cell(row,column-1));
		}
		if(column == 0) {	// prevents column from being highlighted
			cellColor = NORMAL;
			cellBorder = null;
		}
		if(column >= 1 && state.isMispelled(row, column-1)) {
			cellColor = ERROR;

			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					System.out.println("made it");
					if(e.isPopupTrigger()) {
						new SuggestionPopup(r, c, state);
					}
				}
			});
		}

		this.setBackground(cellColor);
		this.setBorder(cellBorder);
		return this;
	}
}
