package client.gui.dataentry;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTabbedPane;

import client.state.BatchState;

@SuppressWarnings("serial")
public class DataPane extends JTabbedPane {
	
	private static final String TABLE_TITLE = "Table Entry";
	private static final String FORM_TITLE = "Form Entry";
	
	private static final int FORM_TAB_INDEX = 1;
	
	private BatchState state;
	
	private TablePane table;	
	private FormPanel form;
	
	public DataPane(BatchState state) {
		this.state = state;
		
		this.setTabPlacement(JTabbedPane.TOP);
		this.table = new TablePane(this.state);
		this.addTab(TABLE_TITLE, table);
		this.form = new FormPanel(this.state);
		this.addTab(FORM_TITLE, this.form);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(DataPane.this.getSelectedIndex() == FORM_TAB_INDEX) {
					form.selectedCellChanged();
				}
			}
		});
	}
	
	
}


