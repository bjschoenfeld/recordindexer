package client.gui.dataentry;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.state.BatchState;
import client.state.Cell;

@SuppressWarnings("serial")
public class SuggestionPopup extends JPopupMenu{
	
	private int row;
	private int column;
	private BatchState state;
	
	private JMenuItem item;

	public SuggestionPopup(int row,	int column, BatchState state) {
		super();
		this.row = row;
		this.column = column;
		this.state = state;
		item = new JMenuItem("See Suggestions");
		this.add(item);
		initListener();
	}

	private void initListener() {
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new SuggestionDialog(); 
			}
			
		});
	}
	
	class SuggestionDialog extends JDialog {
		
		private static final String TITLE = "Suggestions";
		private JPanel mainPanel;
		private JList<String> wordList;
		private JButton cancel;
		private JButton useSuggestion;
		
		public SuggestionDialog() {
			super();
			this.setModal(true);
			this.setSize(250, 200);
			this.setResizable(false);
			this.mainPanel = new JPanel();
			this.setTitle(TITLE);
			this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			this.setLocationRelativeTo(null);
			
			DefaultListModel<String> model = new DefaultListModel<String>();
			Set<String> suggestions;
			suggestions = state.suggestSimilarWords(row, column);
			if(suggestions != null) {
				for(String w: suggestions) {
					model.addElement(w);
				}
			}
			
			wordList = new JList<String>(model);
			wordList.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					if(wordList.isSelectionEmpty()) { 
						useSuggestion.setEnabled(false);
					}
					else { 
						useSuggestion.setEnabled(true);
					}
				}
			});
		    wordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
			cancel = new JButton("Cancel");
			cancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
				
			});
			
			useSuggestion = new JButton("Use Suggestion");
			useSuggestion.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					state.valueChanged(wordList.getSelectedValue(), new Cell(row, column));
					dispose();
				}
			});
			useSuggestion.setEnabled(false);

			JScrollPane scroll = new JScrollPane(this.wordList);
			scroll.setPreferredSize(new Dimension(200, 130));
			mainPanel.add(scroll);
			mainPanel.add(cancel);
			mainPanel.add(useSuggestion);
			
			this.add(mainPanel);
			this.setVisible(true);
		}
	}
	
}
