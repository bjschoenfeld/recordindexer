package client.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel {
	
	private IndexerGUI root;
	
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenuItem downloadItem;
	private JMenuItem logoutItem;
	private JMenuItem exitItem;
	
	private JPanel buttonPanel;
	private JButton zoomInButton;
	private JButton zoomOutButton;
	private JButton invertImageButton;
	private JButton toggleHighlightsButton;
	private JButton saveButton;
	private JButton submitButton;
	
	
	public MenuPanel(IndexerGUI root) {
		this.root = root;
		this.setLayout(new BorderLayout());
		this.menuBar = new JMenuBar();
		this.initMenu();
		this.menuBar.add(fileMenu);
		this.initButtonPanel();
		this.add(menuBar, BorderLayout.NORTH);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	private void initMenu() {
		fileMenu = new JMenu("File");
		
		downloadItem = new JMenuItem("Download Batch");
		downloadItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DownloadBatchDialog dbDialog = new DownloadBatchDialog(root);
				dbDialog.setVisible(true);
			}
		});
		
		logoutItem = new JMenuItem("Logout");
		logoutItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				root.logout();
			}
		});
		
		exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.exit();
			}
		});
		
		fileMenu.add(downloadItem);
		fileMenu.add(logoutItem);
		fileMenu.add(exitItem);
	}
	
	private void initButtonPanel() {
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		zoomInButton = new JButton("Zoom In");
		zoomInButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				root.zoomIn();
			}
		});
		
		zoomOutButton = new JButton("Zoom Out");
		zoomOutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.zoomOut();	
			}
		});
		
		invertImageButton = new JButton("Invert Image");
		invertImageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.invertImage();	
			}
		});
		
		toggleHighlightsButton = new JButton("Toggle Highlights");
		toggleHighlightsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.toggleHighlights();	
			}
		});
		
		saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.save();	
			}
		});
		
		submitButton = new JButton("Submit");
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				root.submit();
				
			}
		});
		
		buttonPanel.add(zoomInButton);
		buttonPanel.add(zoomOutButton);
		buttonPanel.add(invertImageButton);
		buttonPanel.add(toggleHighlightsButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(submitButton);
	}

	public void setBatchDownloaded(boolean batchDownloaded) {
		downloadItem.setEnabled(!batchDownloaded);
		
		zoomInButton.setEnabled(batchDownloaded);
		zoomOutButton.setEnabled(batchDownloaded);
		invertImageButton.setEnabled(batchDownloaded);
		toggleHighlightsButton.setEnabled(batchDownloaded);
		saveButton.setEnabled(batchDownloaded);
		submitButton.setEnabled(batchDownloaded);
	}
	
}
