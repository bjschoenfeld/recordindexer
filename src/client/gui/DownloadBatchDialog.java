package client.gui;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import shared.model.Project;

@SuppressWarnings("serial")
class DownloadBatchDialog extends JDialog {
	
	private IndexerGUI root;
	
	private JPanel mainPanel;
	private JLabel projectLabel;
	private JComboBox<String> projectBox;
	private ArrayList<Project> projects;
	private Project selectedProject;
	private Image selectedImage;
	private JButton viewSampleButton;
	private JButton cancelButton;
	private JButton downloadButton;
	
	public DownloadBatchDialog(IndexerGUI root) {
		super(root, true);
		this.root = root;
		this.initFormat();
		this.initComponenets();
	}
	
	private void initFormat() {
		this.setSize(340, 95);
		this.setLocationRelativeTo(root);
		this.setResizable(false);
		this.setTitle("Download Batch");
	}
	
	private void initComponenets() {
		mainPanel = new JPanel();

		projectLabel = new JLabel("Project:");
		mainPanel.add(projectLabel);
		
		initProjectBox();
		mainPanel.add(projectBox);
		
		initButtons();
		mainPanel.add(viewSampleButton);
		mainPanel.add(cancelButton);
		mainPanel.add(downloadButton);
		
		this.add(mainPanel);
	}
	
	private void initProjectBox() {
		projectBox = new JComboBox<String>();
		projects = root.getProjects();
		if(projects != null) {
			for(Project proj: projects) {
				projectBox.addItem(proj.getTitle());
			}
			selectedProject = projects.get(0);
			projectBox.addActionListener(new ActionListener() {
				@SuppressWarnings("unchecked")
				public void actionPerformed(ActionEvent e) {
					JComboBox<String> cb = (JComboBox<String>)e.getSource();
					String projectTitle = (String)cb.getSelectedItem();
					selectedProject = getProject(projectTitle);
				}
			});
		}
	}
	
	private void initButtons() {
		viewSampleButton = new JButton("View Sample");
		viewSampleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				URL imagePath = root.getSampleImage(selectedProject.getID());
				Image image;
				try {
					image = ImageIO.read(imagePath);
					selectedImage = image.getScaledInstance(500, 350, Image.SCALE_DEFAULT);
				} catch (IOException e1) {
					selectedImage = null;
				}
				JOptionPane.showMessageDialog(DownloadBatchDialog.this, null,
					"Sample image for " + selectedProject.getTitle(), JOptionPane.OK_OPTION,
					new ImageIcon(selectedImage));
			}
		});
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DownloadBatchDialog.this.dispose();					
			}
		});
		
		downloadButton = new JButton("Download");
		downloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DownloadBatchDialog.this.dispose();
				root.downloadBatch(selectedProject.getID());
			}
		});
	}
	
	private Project getProject(String projectTitle) {
		for(Project proj: projects) {
			if(projectTitle.equals(proj.getTitle())) {
				return proj;
			}
		}
		return null;
	}
}

















