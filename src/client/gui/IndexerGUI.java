package client.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import shared.model.Project;
import client.Client;
import client.gui.dataentry.DataPane;
import client.state.BatchState;

@SuppressWarnings("serial")
public class IndexerGUI extends JFrame {
	
	private static final String title = "Record Indexer";
	
	private Client client;
	
	private BatchState batchState;
	
	private JPanel mainPanel;
	private MenuPanel menuPanel;
	private JSplitPane bodyPane;
	private ImagePanel imagePanel;
	private JSplitPane bottomPane;
	private DataPane dataPane;
	private HelpPane helpPane;
	
	public IndexerGUI(Client client) {
		this.client = client;
		this.batchState = client.getBatchState();
		
		this.setTitle(title);
		this.addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent e) {
	            IndexerGUI.this.exit();
	        }
		});
		
		Rectangle windowDimensions = batchState.getWindowDimensions();
		int x = (int) windowDimensions.getX();
		int y = (int) windowDimensions.getY();
		int width = (int) windowDimensions.getWidth();
		int height = (int) windowDimensions.getHeight();
		this.setPreferredSize(new Dimension(width, height));
		this.setLocation(x, y);
		
		menuPanel = new MenuPanel(this);
		menuPanel.setBatchDownloaded(batchState.hasDownloadedBatch());
		
		initBody();
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(menuPanel, BorderLayout.NORTH);
		mainPanel.add(bodyPane, BorderLayout.CENTER);
		
		this.add(mainPanel);
		this.pack();
	}	
	
	private void initBody() {
		dataPane = new DataPane(batchState);
		helpPane = new HelpPane(batchState);
		
		bottomPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, dataPane, helpPane);
		bottomPane.setDividerLocation(batchState.getHorizontalDividerPosition());
		
		imagePanel = new ImagePanel(batchState);
		bodyPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, imagePanel, bottomPane);
		bodyPane.setDividerLocation(batchState.getVerticalDividerPosition());
	}
	
	public void downloadBatch(int projectID) {		
		if(client.downloadBatch(projectID)) {
			menuPanel.setBatchDownloaded(true);
			mainPanel.remove(bodyPane);
			initBody();
			mainPanel.add(bodyPane, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
		}
	}

	public ArrayList<Project> getProjects() {
		return client.getProjects();
	}

	public URL getSampleImage(int projectID) {
		return client.getSampleImage(projectID);
	}

	public void logout() {
		this.save();
		this.client.logout();
	}

	public void exit() {
        this.save();
    	System.exit(0);
	}

	public void zoomIn() {
		this.imagePanel.zoomIn();	
	}

	public void zoomOut() {
		this.imagePanel.zoomOut();		
	}

	public void invertImage() {
		this.imagePanel.invertImage();
	}

	public void toggleHighlights() {
		this.imagePanel.toggleHighlights();
	}

	public void save() {
		batchState.setWindowDimensions(this.getBounds());
		batchState.setVerticalDividerPosition(bodyPane.getDividerLocation());
		batchState.setHorizontalDividerPosition(bottomPane.getDividerLocation());
		imagePanel.save();
		
//		ArrayList<BatchStateListener> listeners = this.batchState.getBatchStateListeners();
//		this.batchState.prepareToSave();
		BatchState.save(this.batchState);
//		for(BatchStateListener l: listeners) {
//			this.batchState.addListener(l);
//		}
	}

	public void submit() {
		if(client.submitBatch()) {
			menuPanel.setBatchDownloaded(false);
			mainPanel.remove(bodyPane);
			initBody();
			mainPanel.add(bodyPane, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
		}
	}
	
}
