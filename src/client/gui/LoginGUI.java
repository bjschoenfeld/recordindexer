package client.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import shared.model.User;
import client.Client;

@SuppressWarnings("serial")
public class LoginGUI extends JFrame {
	
	private Client client;
	
	private JPanel mainPanel;
	
	private JLabel userLabel;
	private JTextField userField;
	private JLabel passwordLabel;
	private JTextField passwordField;
	private JButton loginButton;
	private JButton exitButton;
	
	public LoginGUI(Client client) {	
		this.client = client;
		this.format();
		this.initComponents();
	}
	
	private void format() {
		this.setTitle("Login to Indexer");
		this.setSize(400, 140);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void initComponents() {
		this.setLayout(new BorderLayout());
		mainPanel = new JPanel();
		
		mainPanel.setLayout(null);
		
		userLabel = new JLabel("Username:");
		userLabel.setBounds(10, 5, 80, 25);
		mainPanel.add(userLabel);
		
		userField = new JTextField(25);
		userField.setBounds(100, 5, 280, 25);
		mainPanel.add(userField);
		
		passwordLabel = new JLabel("Password:");
		passwordLabel.setBounds(10, 40, 80, 25);
		mainPanel.add(passwordLabel);
		
		passwordField = new JPasswordField(25);
		passwordField.setBounds(100, 40, 280, 25);
		mainPanel.add(passwordField);
		
		loginButton = new JButton("Login");
		loginButton.setBounds(85, 80, 100, 25);
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {			
				User user = client.validateUser(userField.getText(), passwordField.getText()); 
				new AfterLoginDialog(LoginGUI.this, user);
				if(user != null) {
					client.login(user);
				}
			}
		});
		mainPanel.add(loginButton);
		
		exitButton = new JButton("Exit");
		exitButton.setBounds(205, 80, 100, 25);
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		mainPanel.add(exitButton);
		
		this.add(mainPanel, BorderLayout.CENTER);
	}

	class AfterLoginDialog extends JDialog {
		
		private JPanel mainPanel;
		private JLabel messageLabel;
		private JButton okButton;
		private User user;
		
		public AfterLoginDialog(LoginGUI owner, User user) {
			super(owner, true);
			
			this.setSize(300, 120);
			this.setLocationRelativeTo(owner);
			this.setResizable(false);
			
			this.user = user;
			if(user != null) {
				this.setTitle("Welcome to Indexer");
			}
			else {
				this.setTitle("Login Failed");
			}
			
			this.setLayout(new BorderLayout());
			
			mainPanel = new JPanel();
			mainPanel.setLayout(null);
	
			this.setMessageLabel();
			mainPanel.add(messageLabel);
			
			this.setOkButton();
			mainPanel.add(okButton);
			
			this.add(mainPanel, BorderLayout.CENTER);
			this.setVisible(true);
		}
		
		private void setMessageLabel() {
			String message = "Invalid username and/or password";
			if(user != null) {
				String name = user.getFirstname() + " " + user.getLastname();
				int indexedRecords = user.getIndexedRecords();
				message = "<html>Welcome, " + name + ".<br>" + 
						  "You have indexed " + indexedRecords + " records.</html>";
			}
			messageLabel = new JLabel(message);
			messageLabel.setBounds(15, 10, 280, 25);
		}
		
		private void setOkButton() {
			okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					AfterLoginDialog.this.dispose();
				}
			});
			okButton.setBounds(120, 50, 60, 30);
		}
	}
	
}
