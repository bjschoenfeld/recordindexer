package client;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import shared.communication.*;

public class ClientCommunicator {
		
	private static final String DEFAULT_SERVER_HOST = "localhost";
	private static final int DEFAULT_SERVER_PORT = 8081;
	private static final String slash = File.separator;
	private static final String DEFAULT_URL_PREFIX = "http:" + slash + slash + 
													 DEFAULT_SERVER_HOST + ":" + 
													 DEFAULT_SERVER_PORT;
	
	private static final String HTTP_POST = "POST";
	
	private String urlPrefix;

	private XStream xmlStream;
	
	public ClientCommunicator() {
		xmlStream = new XStream(new DomDriver());
		urlPrefix = DEFAULT_URL_PREFIX;
	}
	
	public ClientCommunicator(String serverHost, String serverPort) {
		xmlStream = new XStream(new DomDriver());
		urlPrefix = "http:" + slash + slash + serverHost + ":" + serverPort;
	}
	
	/**
	 * Sends a request to the server to validate input.username and input.password with the database of users.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public UserResult validateUser(String username, String password)  {
		UserParam param = new UserParam(username, password);
		UserResult result = new UserResult();
		try {
			result = (UserResult)doPost("/ValidateUser", param);
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}
	
	/**
	 * Attempts to retrieve all the available projects from the server. Will fail if user is not valid.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public ProjectResult getProjects(String username, String password)  {
		UserParam param = new UserParam(username, password);
		ProjectResult result = new ProjectResult();
		try {
			result = (ProjectResult)doPost("/GetProjects", param);
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}
	
	/**
	 * Attempts to retrieve the url of a sample image for a project. Will fail if user is not valid.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public SampleImageResult getSampleImage(String username, String password, int projectID) {
		ProjectParam param = new ProjectParam(username, password, projectID);
		SampleImageResult result = new SampleImageResult();
		try {
			result = (SampleImageResult)doPost("/GetSampleImage", param);
			if(result.getStatus()) {
				String imagePath = urlPrefix + File.separator + result.getFile();
				result.setFile(imagePath);
			}
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}

	/**
	 * Attempts to retrieve the information associated with a batch.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public DownloadBatchResult downloadBatch(String username, String password, int projectID) {
		ProjectParam param = new ProjectParam(username, password, projectID);
		DownloadBatchResult result = new DownloadBatchResult();
		try {
			result = (DownloadBatchResult)doPost("/DownloadBatch", param);
			if(result.getStatus()) {
				result.prependFilePrefix(urlPrefix + File.separator);
			}
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}

	/**
	 * Attempts to submit a completed indexed record.
	 * If successful, the user gets credit for indexing all the records in the batch
	 * @param input Container class.
	 * @return Whether successful.
	 * @throws ClientException 
	 */
	public SubmitBatchResult submitBatch(String username, String password, int batchID, String fieldValues) {
		SubmitBatchParam param = new SubmitBatchParam(username, password, batchID, fieldValues);
		SubmitBatchResult result = new SubmitBatchResult();
		try {
			result = (SubmitBatchResult)doPost("/SubmitBatch", param);
		}
		catch (ClientException e) {
			e.printStackTrace();
			result.fail();
		}
		return result;
	}

	/**
	 * Attempts to retrieve all of the fields for a given project.
	 * If input.projectID is the empty string, attempts to retrive all fields for all projects.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public FieldResult getFields(String username, String password, int projectID) {
		ProjectParam param = new ProjectParam(username, password, projectID);
		FieldResult result = new FieldResult();
		try {
			result = (FieldResult)doPost("/GetFields", param);
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}

	/**
	 * Attempts to find the any passed substring in any of the passed fields.
	 * @param input Container class.
	 * @return Container class.
	 * @throws ClientException 
	 */
	public SearchResult search(SearchParam input)  {
		SearchResult result = new SearchResult();
		try {
			result = (SearchResult)doPost("/Search", input);
			if(result.getStatus().equals("TRUE"))	{
				Map<Integer, String> images = result.getImages();
				for(int key: images.keySet()) {
					String image = images.get(key);
					images.put(key, urlPrefix + File.separator + image);
				}
				result.setImages(images);
			}
		}
		catch (ClientException e) {
			result.fail();
		}
		return result;
	}
	
	private Object doPost(String context, Object param) throws ClientException  {
		try {
			URL url = new URL(urlPrefix + context);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(HTTP_POST);
			connection.setDoOutput(true);
			connection.connect();
			xmlStream.toXML(param, connection.getOutputStream());
			connection.getOutputStream().close();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				Object result = xmlStream.fromXML(connection.getInputStream());
				return result;
			}
			else {
				throw new ClientException(String.format("doPost failed: %s (http code %d)",
						context, connection.getResponseCode()));
			}
		}
		catch (IOException e) {
			throw new ClientException(String.format("doPost failed: %s", e.getMessage()), e);
		}
	}
	
}
