package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Batch;


public class BatchDAO {
	
	private Database db;
	
	/**
	 * Creates a reference to the database.
	 * @param db
	 */
	public BatchDAO(Database db) {
		this.db = db;
	}
	
	/**
	 * Creates the table BATCHES. Will overwrite the BACTHES table if it exists.
	 * @throws DatabaseException
	 */
	public void createTable() throws DatabaseException {
		String fields = "BATCHID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, " +
						"PARENTPROJECTID INTEGER NOT NULL, " +
						"FILE TEXT NOT NULL, " +
						"STATUS INTEGER NOT NULL";
		DAO.createTable(db, "BATCHES", fields);
	}
	
	/**
	 * Adds the batch to the BATCHES table. Also adds all the cells within the
	 * batch, if there are any.
	 * @param batch
	 * @throws DatabaseException
	 */
	public int add(Batch batch) throws DatabaseException {
		String fieldLabels = "PARENTPROJECTID, FILE, STATUS";
		int primaryKey = DAO.add(db, "BATCHES", fieldLabels, batch.toSQLString(), true);
		return primaryKey;
	}
	
	/**
	 * Retrieves the batch with the given ID.
	 * @param batchID
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException
	 */
	public Batch getBatch(int batchID) throws DatabaseException, NotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		Batch result = new Batch();
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM BATCHES WHERE BATCHID = " + batchID + ";";
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				result.setID(rs.getInt(1));
				result.setParentProjectID(rs.getInt(2));
				result.setFile(rs.getString(3));
				result.setStatus(rs.getString(4));
			}
			else throw new NotFoundException("Batch " + batchID + " not found.");
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
		return result;
	}
	
	/**
	 * Retrieves the first (available) batch with the given projectID.
	 * @param projectID
	 * @param mustBeAvailable
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException
	 */
	public Batch getBatch(int projectID, boolean mustBeAvailable) throws DatabaseException, NotFoundException {
		String sql = "SELECT * FROM BATCHES WHERE PARENTPROJECTID = " + projectID + " LIMIT 1;";
		if(mustBeAvailable) {
			sql = "SELECT * FROM BATCHES WHERE PARENTPROJECTID = " + 
				  projectID + " AND STATUS = 'AVAILABLE'  LIMIT 1;";
		}
		
		Statement stmt = null;
		ResultSet rs = null;
		Batch result = new Batch();
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				result.setID(rs.getInt(1));
				result.setParentProjectID(rs.getInt(2));
				result.setFile(rs.getString(3));
				result.setStatus(rs.getString(4));
			}
			else {
				throw new NotFoundException("No batch found that matches the parameters");
			}
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
		return result;
	}
	
	/**
	 * Sets the batch with batchID to the passed-in status.
	 * @param batchID
	 * @param status
	 * @throws DatabaseException
	 */
	public void setStatus(int batchID, String status) throws DatabaseException {
		String sql = "UPDATE BATCHES SET STATUS = '" + status + "' WHERE BATCHID = " + batchID + ";";
		
		Statement stmt = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
		
	}
	

}
