package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Project;

public class ProjectDAO {
	
	private Database db;

	/**
	 * Creates a reference to the database.
	 * @param db
	 */
	public ProjectDAO(Database db) {
		this.db = db;
	}
	
	/**
	 * Creates the table PROJECTS. Will overwrite the PROJECTS table if it exists.
	 * @throws DatabaseException
	 */
	public void createTable() throws DatabaseException {
		String fields = "PROJECTID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, " +
						"TITLE TEXT NOT NULL, " +
						"RECORDSPERBATCH INTEGER NOT NULL, " +
						"FIRSTYCOORD INTEGER NOT NULL, " +
						"RECORDHEIGHT INTEGER NOT NULL";
		DAO.createTable(db, "PROJECTS", fields);
	}

	/**
	 * Adds the project to the PROJECTS table. Also adds any fields and batches
	 * within the project to their respective tables.
	 * @param project
	 * @throws DatabaseException
	 */
	public int add(Project project) throws DatabaseException {
		String fieldLabels = "TITLE, RECORDSPERBATCH, FIRSTYCOORD, RECORDHEIGHT";
		int primaryKey = DAO.add(db, "PROJECTS", fieldLabels, project.toSQLString(), true);
		return primaryKey;
	}
	
	/**
	 * Retrieves all the projects.
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException 
	 */
	public ArrayList<Project> getAllProjects() throws DatabaseException, NotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM PROJECTS;";
			rs = stmt.executeQuery(sql);
			ArrayList<Project> projects = new ArrayList<Project>();
			while(rs.next()) {
				projects.add(new Project(rs.getInt(1), rs.getString(2), rs.getInt(3),
										 rs.getInt(4), rs.getInt(5)));
			}
			if(projects.isEmpty()) throw new NotFoundException("No projects exist");
			return projects;
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
	}
	
	public Project getProject(int projectID) throws DatabaseException {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM PROJECTS WHERE PROJECTID = " + projectID + ";";
			rs = stmt.executeQuery(sql);
			Project project = null;
			if(rs.next()) {
				project = new Project(rs.getInt(1), rs.getString(2), rs.getInt(3),
										 rs.getInt(4), rs.getInt(5));
			}
			return project;
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
	}
}
