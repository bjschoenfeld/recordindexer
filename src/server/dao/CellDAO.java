package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Cell;

public class CellDAO {
	
	private static final String tableName = "CELLS";
	private Database db;

	/**
	 * Creates a reference to the databse.
	 * @param db
	 */
	public CellDAO(Database db) {
		this.db = db;
	}

	/**
	 * Creates the table CELLS. Will overwrite the CELLS table if it exists.
	 * @throws DatabaseException
	 */
	public void createTable() throws DatabaseException {
		String fields = "CELLID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, " +
						"DATA TEXT NOT NULL, " +
						"PARENTFIELDID INTEGER NOT NULL, " +
						"PARENTBATCHID INTEGER NOT NULL, " +
						"RECORDNUMBER INTEGER NOT NULL";
		DAO.createTable(db, tableName, fields);
	}
	
	/**
	 * Adds a cell to the CELLS table.
	 * @param cell
	 * @throws DatabaseException
	 */
	public int add(Cell cell) throws DatabaseException{
		String fields = "DATA, PARENTFIELDID, PARENTBATCHID, RECORDNUMBER";
		int primaryKey = DAO.add(db, tableName, fields, cell.toSQLString(), true);
		return primaryKey;
	}

	private String prepareData(String[] values) {
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < values.length; i++) {
			if(i > 0) result.append(", ");
			if(values[i].matches("\\d+")) result.append(values[i]);
			else result.append("'" + values[i] + "'");
		}
		return result.toString();
	}
	/**
	 * Searches the CELLS table for any cells that have the given fieldID and data.
	 * @param fieldID
	 * @param data
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException 
	 */
	public ArrayList<Cell> search(String[] fieldIDs, String[] values) throws DatabaseException, NotFoundException {
		Statement stmt = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM CELLS WHERE " +
						 "PARENTFIELDID IN (" + prepareData(fieldIDs) + ") " + 
						 "AND DATA IN (" + prepareData(values) + ") " +
						 "COLLATE NOCASE;";
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<Cell> cells = new ArrayList<Cell>();
			while(rs.next()) {
				cells.add(new Cell(rs.getInt(1), rs.getString(2), rs.getInt(3),
								rs.getInt(4), rs.getInt(5)));
			}
			if(cells.isEmpty()) throw new NotFoundException("No match found.");
			
			return cells;
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
		
	}
}
