package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.User;

public class UserDAO {
	
	private Database db;
	
	/**
	 * Creates a reference to the database.
	 * @param db
	 */
	public UserDAO(Database db) {
		this.db = db;
	}
	
	/**
	 * Creates the table PROJECTS. Will overwrite the PROJECTS table if it exists.
	 * @throws DatabaseException
	 */
	public void createTable() throws DatabaseException {
		String fields = "USERNAME TEXT PRIMARY KEY NOT NULL UNIQUE, " + 
				 		"PASSWORD TEXT NOT NULL, " + 
						"FIRSTNAME TEXT NOT NULL, " + 
						"LASTNAME TEXT NOT NULL, " + 
						"EMAIL TEXT NOT NULL, " + 
						"INDEXEDRECORDS INTEGER NOT NULL DEFAULT 0, " + 
						"CURRENTBATCH INTEGER NOT NULL DEFAULT 0";
		DAO.createTable(db, "USERS", fields);
	}

	/**
	 * Adds the user to the database.
	 * @param user
	 * @throws DatabaseException
	 */
	public void add(User user) throws DatabaseException {
		String fields = "USERNAME, PASSWORD, FIRSTNAME, LASTNAME, EMAIL, INDEXEDRECORDS, CURRENTBATCH";
		DAO.add(db, "USERS", fields, user.toSQLString(), false);
	}
	
	/**
	 * Retrieves the user with the given username and password.
	 * @param username
	 * @param password
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException 
	 */
	public User validateUser(String username, String password) throws DatabaseException, NotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM USERS WHERE " +
						 "USERNAME = '" + username + "' AND " +
						 "PASSWORD = '" + password + "';";
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				return new User(rs.getString(1), rs.getString(2), rs.getString(3),
								rs.getString(4), rs.getString(5), rs.getInt(6), 
								rs.getInt(7));
			}
			else throw new NotFoundException("Not a valid username/password combination");
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
	}

	/**
	 * Increases the number of records indexed for the given user.
	 * @param username
	 * @param indexedRecords
	 * @throws DatabaseException
	 */
	public void increaseRecordsIndexed(String username, int indexedRecords) 
			throws DatabaseException 
	{
		Statement stmt = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "UPDATE USERS SET INDEXEDRECORDS = INDEXEDRECORDS + " +
						 indexedRecords + " WHERE USERNAME = '" + username + "';";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
	}
	
	/**
	 * Sets the user's current batch to the given one.
	 * @param username
	 * @param batchID
	 * @throws DatabaseException
	 */
	public void setCurrentBatch(String username, int batchID) 
			throws DatabaseException 
	{
		Statement stmt = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "update users set currentbatch = " + batchID +
						 " where username = '" + username + "';";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
	}
}
