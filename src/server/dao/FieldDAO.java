package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import server.database.Database;
import server.database.DatabaseException;
import shared.model.Field;

public class FieldDAO {

	private Database db;

	/**
	 * Creates a reference to the database.
	 * @param db
	 */
	public FieldDAO(Database db) {
		this.db = db;
	}
	
	/**
	 * Creates the table FIELDS. Will overwrite the FIELDS table if it exists.
	 * @throws DatabaseException
	 */
	public void createTable() throws DatabaseException {
		String fields = "FIELDID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, " +
						"PARENTPROJECTID INTEGER NOT NULL, " +
						"POSITION INTEGER NOT NULL, " +
						"TITLE TEXT NOT NULL, " +
						"XCOORD INTEGER NOT NULL, " +
						"WIDTH INTEGER NOT NULL, " +
						"HELPHTML TEXT NOT NULL, " +
						"KNOWNDATA TEXT";
		DAO.createTable(db, "FIELDS", fields);
	}
	
	/**
	 * Adds the field to the FIELDS table.
	 * @param field
	 * @throws DatabaseException
	 */
	public int add(Field field) throws DatabaseException {
		String fields = "PARENTPROJECTID, POSITION, TITLE, XCOORD, WIDTH, HELPHTML, KNOWNDATA";
		int primaryKey = DAO.add(db, "FIELDS", fields, field.toSQLString(), true);
		return primaryKey;
	}
	
	/**
	 * Retrieves all the fields that have the given projectID.
	 * @param projectID
	 * @return
	 * @throws DatabaseException
	 * @throws NotFoundException 
	 */
	public ArrayList<Field> getFields(int projectID) throws DatabaseException, NotFoundException {
		Statement stmt = null;
		ResultSet rs = null;
		try {	
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM FIELDS WHERE PARENTPROJECTID = " + 
						 projectID + ";";
			rs = stmt.executeQuery(sql);
			ArrayList<Field> fields = new ArrayList<Field>();
			while(rs.next()) {
				fields.add(new Field(rs.getInt(1), rs.getInt(2), rs.getInt(3),
									 rs.getString(4), rs.getInt(5), rs.getInt(6), 
									 rs.getString(7), rs.getString(8)));
			}
			if(fields.isEmpty()) {
				throw new NotFoundException("No fields with projectID: " + projectID);
			}
			return fields;
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(rs);
			Database.safeClose(stmt);
		}
	}
}
