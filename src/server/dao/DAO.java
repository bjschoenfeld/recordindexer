package server.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import server.database.Database;
import server.database.DatabaseException;

public class DAO {
	
	/**
	 * Creates a table in the database with the given fields.
	 * Overwrites any existing table with the same name.
	 * @param db
	 * @param tableName
	 * @param fields
	 * @throws DatabaseException
	 */
	public static void createTable(Database db, String tableName, String fields) throws DatabaseException {
		Statement stmt = null;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "DROP TABLE IF EXISTS " + tableName + "; " +
						 "CREATE TABLE " + tableName + " (" + fields + ");";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
	}
	
	/**
	 * Adds a row to the table in the database.
	 * @param db
	 * @param tableName
	 * @param row
	 * @throws DatabaseException
	 */
	public static int add(Database db, String tableName, String fields, String row, boolean returnGeneratedKey) throws DatabaseException {
		Statement stmt = null;
		int primaryKey = 0;
		try {
			Connection conn = db.getConnection();
			stmt = conn.createStatement();
			String sql = "INSERT INTO " + tableName + "(" + fields + ") " + "VALUES (" + row + ");";
			stmt.executeUpdate(sql);
			if(returnGeneratedKey) {
				ResultSet rs = stmt.executeQuery("SELECT last_insert_rowid()");
				primaryKey = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			Database.safeClose(stmt);
		}
		return primaryKey;
	}

}
