package server.database;

import java.io.*;
import java.sql.*;

import server.dao.BatchDAO;
import server.dao.CellDAO;
import server.dao.FieldDAO;
import server.dao.ProjectDAO;
import server.dao.UserDAO;

public class Database {	
	
	private static final String DATABASE_DIRECTORY = "database";
	private static final String DATABASE_FILE = "recordindexer.sqlite";
	private static final String DATABASE_URL = "jdbc:sqlite:" + DATABASE_DIRECTORY + File.separator + DATABASE_FILE;
	
	private Connection connection;
	
	private BatchDAO batchDao;
	private CellDAO cellDao;
	private FieldDAO fieldDao;
	private ProjectDAO projectDao;
	private UserDAO userDao;
	
	public Database() {
		connection = null;
		batchDao = new BatchDAO(this);
		cellDao = new CellDAO(this);
		fieldDao = new FieldDAO(this);
		projectDao = new ProjectDAO(this);
		userDao = new UserDAO(this);
	}
	
	public static void initialize() throws DatabaseException {
		try {
			final String driver = "org.sqlite.JDBC";
			Class.forName(driver);
		}
		catch(ClassNotFoundException e) {
			DatabaseException serverEx = new DatabaseException(
					"Could not load database driver", e);
			throw serverEx; 
		}
	}
	
	public Connection getConnection() {
		return connection;
	}

	public void startTransaction() throws DatabaseException {
		try {
			assert (connection == null);	
			connection = DriverManager.getConnection(DATABASE_URL);
			connection.setAutoCommit(false);
		}
		catch (SQLException e) {
			throw new DatabaseException("Could not connect to database. Make sure " + 
				DATABASE_FILE + " is available in ." + File.separator + DATABASE_DIRECTORY, e);
		}
	}
	
	public void endTransaction(boolean commit) {
		if (connection != null) {		
			try {
				if (commit) {
					connection.commit();
				}
				else {
					connection.rollback();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				safeClose(connection);
				connection = null;
			}
		}
	}
	
	public static void safeClose(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			}
			catch (SQLException e) {
				// ...
			}
		}
	}
	
	public static void safeClose(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException e) {
				// ...
			}
		}
	}
	
	public static void safeClose(PreparedStatement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException e) {
				// ...
			}
		}
	}
	
	public static void safeClose(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			}
			catch (SQLException e) {
				// ...
			}
		}
	}
	
	public UserDAO getUserDAO() {
		return userDao;
	}
	
	public ProjectDAO getProjectDAO() {
		return projectDao;
	}
	
	public FieldDAO getFieldDAO() {
		return fieldDao;
	}
	
	public BatchDAO getBatchDAO() {
		return batchDao;
	}
	
	public CellDAO getCellDAO() {
		return cellDao;
	}
}
