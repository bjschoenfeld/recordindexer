package server;


import java.io.*;
import java.net.*;

import com.sun.net.httpserver.*;

import server.facade.*;
import server.handler.*;

public class Server {
	
	private static final int DEFAULT_SERVER_PORT_NUMBER = 8081;
	private static final int MAX_WAITING_CONNECTIONS = 10;
	
	private HttpServer server;
	private int portNumber;
	
	private Server() {
		portNumber = DEFAULT_SERVER_PORT_NUMBER;
	}
	
	private Server(int portNumber) {
		this.portNumber = portNumber;
	}
	
	private void run() throws ServerException {		
		ServerFacade.initialize();		
		try {
			server = HttpServer.create(new InetSocketAddress(portNumber), MAX_WAITING_CONNECTIONS);
		} 
		catch (IOException e) {			
			throw new ServerException(e);
		}

		server.setExecutor(null); // use the default executor
		
		server.createContext("/ValidateUser", new ValidateUserHandler());
		server.createContext("/GetProjects", new GetProjectsHandler());
		server.createContext("/GetSampleImage", new GetSampleImageHandler());
		server.createContext("/DownloadBatch", new DownloadBatchHandler());
		server.createContext("/SubmitBatch", new SubmitBatchHandler());
		server.createContext("/GetFields", new GetFieldsHandler());
		server.createContext("/Search", new SearchHandler());
		server.createContext("/", new DownloadFileHandler());

		server.start();
	}
	
	public static void main(String[] args) {
		if(args != null && args.length == 1)	{
			int port = Integer.parseInt(args[0]);
			try {
				new Server(port).run();
			} catch (ServerException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				new Server().run();
			} catch (ServerException e) {
				e.printStackTrace();
			}
		}
	}
	
}
