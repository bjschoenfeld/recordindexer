package server.handler;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import com.sun.net.httpserver.*;

public class DownloadFileHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String rawFilePath = exchange.getRequestURI().toString();
		File file = new File(rawFilePath.substring(1));
		byte[] bytes = Files.readAllBytes(file.toPath());
		exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		exchange.getResponseBody().write(bytes);
		exchange.getResponseBody().close();
	}

}
