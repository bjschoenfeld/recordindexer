package server.handler;

import java.io.IOException;
import java.net.HttpURLConnection;

import server.ServerException;
import server.facade.ServerFacade;
import shared.communication.*;
import com.sun.net.httpserver.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class ValidateUserHandler implements HttpHandler {
	
	private XStream xmlStream = new XStream(new DomDriver());	

	public void handle(HttpExchange exchange) throws IOException {
		
		UserResult result = new UserResult();
		
		try {
			UserParam userParam = (UserParam)xmlStream.fromXML(exchange.getRequestBody());
			result = ServerFacade.validateUser(userParam);
		}
		catch (ServerException e) {
			exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, -1);
			result.fail();
		}
		
		exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		xmlStream.toXML(result, exchange.getResponseBody());
		exchange.getResponseBody().close();
	}
}
