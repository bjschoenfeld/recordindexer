package server.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import server.ServerException;
import server.dao.NotFoundException;
import server.database.Database;
import server.database.DatabaseException;
import shared.communication.DownloadBatchResult;
import shared.communication.FieldResult;
import shared.communication.ProjectParam;
import shared.communication.ProjectResult;
import shared.communication.SampleImageResult;
import shared.communication.SearchParam;
import shared.communication.SearchResult;
import shared.communication.SubmitBatchParam;
import shared.communication.SubmitBatchResult;
import shared.communication.UserParam;
import shared.communication.UserResult;
import shared.model.Batch;
import shared.model.Cell;
import shared.model.Field;
import shared.model.Project;
import shared.model.User;

public class ServerFacade {
	
	private static final int DEFAULT_BATCH = 0;
	
	public static void initialize() throws ServerException {		
		try {
			Database.initialize();
		}
		catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}		
	}
	
	public static UserResult validateUser(UserParam userParam) throws ServerException {	

		UserResult result = new UserResult();
		
		Database db = new Database();		
		try {
			db.startTransaction();
			User user = db.getUserDAO().validateUser(userParam.getUsername(), userParam.getPassword());
			db.endTransaction(true);
			result.succeed(user);
		} 
		catch (DatabaseException e) {
			throw new ServerException(e);
		} 
		catch (NotFoundException e) {
			result.invalidate();
		} 
		finally {
			db.endTransaction(false);
		}
		
		return result;
	}
	
	public static ProjectResult getProjects(UserParam userParam) throws ServerException {
		
		ProjectResult result = new ProjectResult();
		
		UserResult userResult = validateUser(userParam);
		if(userResult.getStatus()) {
			
			ArrayList<Project> projects = null;
			
			Database db = new Database();
			try {
				db.startTransaction();
				projects = db.getProjectDAO().getAllProjects();
				db.endTransaction(true);
				result.succeed(projects);
			} 
			catch (DatabaseException e) {
				throw new ServerException(e);
			} 
			catch (NotFoundException e) {
				result.fail();
			}
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;		
	}
	
	public static SampleImageResult getSampleImage(ProjectParam param) throws ServerException {
		SampleImageResult result = new SampleImageResult();
		
		UserResult userResult = validateUser(
				new UserParam(param.getUsername(), param.getPassword()));
		if(userResult.getStatus()) {
			
			Batch batch = null;
			
			Database db = new Database();
			try {
				db.startTransaction();	
				batch = db.getBatchDAO().getBatch(param.getProjectID(), false);
				db.endTransaction(true);
				String imagePath = batch.getFile();
				result.succeed();
				result.setFile(imagePath);
			} 
			catch (DatabaseException e) {
				throw new ServerException(e.getMessage(), e);
			} 
			catch (NotFoundException e) {
				result.fail();
			}
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;
	}
	
	public static DownloadBatchResult downloadBatch(ProjectParam param) throws ServerException {
		DownloadBatchResult result = new DownloadBatchResult();

		UserResult userResult = validateUser(
				new UserParam(param.getUsername(), param.getPassword()));
		//check if user already has a batch. 
			//is this a way for the user to return a batch without indexing it? 
		if(userResult.getStatus() && userResult.getUser().getCurrentBatch() == DEFAULT_BATCH) {
			Project project = null;
			ArrayList<Field> fields = null;
			Batch batch = null;
			
			Database db = new Database();
			try {
				db.startTransaction();	
				project = db.getProjectDAO().getProject(param.getProjectID());
				fields = db.getFieldDAO().getFields(param.getProjectID());
				batch = db.getBatchDAO().getBatch(param.getProjectID(), true);
				db.getBatchDAO().setStatus(batch.getID(), "CHECKEDOUT");
				db.getUserDAO().setCurrentBatch(param.getUsername(), batch.getID());
				db.endTransaction(true);
				result.succeed(project, fields, batch);
			} 
			catch (DatabaseException e) {
				throw new ServerException(e.getMessage(), e);
			} 
			catch (NotFoundException e) {
				result.fail();
			}	
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;	
	}
	
	private static int getParentProjectID(int batchID) throws ServerException, NotFoundException {
		int projectID = 0;
		
		Database db = new Database();
		try {
			db.startTransaction();
			Batch batch = db.getBatchDAO().getBatch(batchID);
			db.endTransaction(true);
			projectID = batch.getParentProjectID();
		} 
		catch(DatabaseException e) {
			throw new ServerException(e.getMessage(), e);
		}
		finally {
			db.endTransaction(false);
		}
		
		return projectID;
	}
	
	private static ArrayList<Cell> extractValues(SubmitBatchParam param) throws ServerException, NotFoundException {
		ArrayList<Cell> results = new ArrayList<Cell>();
		
		int batchID = param.getBatchID();
		int projectID = getParentProjectID(batchID);
		ArrayList<Field> fields = getOrderedFields(projectID);
		String[] records = param.getFieldValues().split(";", -1);
		for(int i = 0; i < records.length; i++) {
			
			String[] values = records[i].split(",", -1);
			for(int j = 0; j < values.length; j++) {
				
				String value = values[j];
				int parentFieldID = fields.get(j).getID();
				int recordNumber = i;
				Cell cell = new Cell(value, parentFieldID, batchID, recordNumber);
				
				results.add(cell);
			}
		}
		
		return results;
	}
	
	private static String getBatchStatus(int batchID) throws DatabaseException, NotFoundException {
		Batch batch = new Batch();		
		Database db = new Database();
		try {
			db.startTransaction();
			batch = db.getBatchDAO().getBatch(batchID);
			db.endTransaction(true);
		}
		finally {
			db.endTransaction(false);
		}
		return batch.getStatus();
	}
	
	public static SubmitBatchResult submitBatch(SubmitBatchParam param) throws ServerException {
		SubmitBatchResult result = new SubmitBatchResult();
		
		UserResult userResult = validateUser(new UserParam(param.getUsername(), param.getPassword()));
		if(userResult.getStatus()) {

			String batchStatus;
			try {
				batchStatus = getBatchStatus(param.getBatchID());
			} 
			catch (DatabaseException | NotFoundException e) {
				e.printStackTrace();
				throw new ServerException(e);
			}
			
			if(!batchStatus.equals("CHECKEDOUT")) {
				throw new ServerException("Batch cannot be submitted");
			}
			
			Database db = new Database();
			try {
				ArrayList<Cell> cells = extractValues(param);
				int numberOfRecords = param.getFieldValues().split(";", -1).length;
				db.startTransaction();
				for(Cell cell: cells) {
					db.getCellDAO().add(cell);
				}
				db.getUserDAO().increaseRecordsIndexed(param.getUsername(), numberOfRecords);
				db.getUserDAO().setCurrentBatch(param.getUsername(), DEFAULT_BATCH);
				db.getBatchDAO().setStatus(param.getBatchID(), "SUBMITTED");
				db.endTransaction(true);
				result.succeed();
			} 
			catch (DatabaseException e) {
				throw new ServerException(e.getMessage(), e);
			} 
			catch (NotFoundException e) {
				e.printStackTrace();
				result.fail();
			}
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;
	}

	private static ArrayList<Field> getOrderedFields(int projectID) throws ServerException, NotFoundException {
		
		ArrayList<Field> fields = null;
		
		Database db = new Database();
		try {
			db.startTransaction();
			fields = db.getFieldDAO().getFields(projectID);
			db.endTransaction(true);
		} 
		catch(DatabaseException e) {
			throw new ServerException(e.getMessage(), e);
		} 
		finally {
			db.endTransaction(false);
		}
		
		ArrayList<Field> sortedFields = new ArrayList<Field>();
		for(int i = 0; i < fields.size(); i++) {
			sortedFields.add(null);
		}
		for(Field field: fields) {
			int index = field.getPosition() - 1;
			try {
				sortedFields.set(index, field);
			}
			catch (Exception e) {
				throw new ServerException(e);
			}
		}
		return sortedFields;
	}
	
	public static FieldResult getFields(ProjectParam param) throws ServerException {
		FieldResult result = new FieldResult();
		
		UserResult userResult = validateUser(new UserParam(param.getUsername(), param.getPassword()));
		if(userResult.getStatus()) {
			Database db = new Database();
			try {
				ArrayList<Field> fields = new ArrayList<Field>();
				if(param.getProjectID() == 0) {
					db.startTransaction();
					ArrayList<Project> projects = db.getProjectDAO().getAllProjects();
					db.endTransaction(true);
					for(Project proj: projects) {
						fields.addAll(getOrderedFields(proj.getID()));
					}
				}
				else {
					fields = getOrderedFields(param.getProjectID());
				}
				result.succeed(fields);
			} 
			catch (NotFoundException e) {
				result.fail();
			}
			catch (DatabaseException e) {
				throw new ServerException(e);
			}
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;
	}
	
	public static SearchResult search(SearchParam param) throws ServerException {
		SearchResult result = new SearchResult();
		
		UserResult userResult = validateUser(
				new UserParam(param.getUsername(), param.getPassword()));
		if(userResult.getStatus()) {
			ArrayList<Cell> cells = null;
			Map<Integer, String> images = null;
			
			String[] fieldIDs = param.getFieldIDs().split(",", -1);
			String[] searchValues = param.getSearchValues().split(",", -1);
			
			Database db = new Database();
			try {
				db.startTransaction();
				cells = db.getCellDAO().search(fieldIDs, searchValues);
				images = new HashMap<Integer, String>();
				for(Cell cell: cells) {
					String image = db.getBatchDAO().getBatch(cell.getParentBatchID()).getFile();
					images.put(cell.getParentBatchID(), image);
				}
				db.endTransaction(true);
				result.succeed(cells);
				result.setImages(images);
			} 
			catch (DatabaseException e) {
				throw new ServerException(e);
			} 
			catch (NotFoundException e) {
				result.fail();
			}
			finally {
				db.endTransaction(false);
			}
		}
		else {
			result.fail();
		}
		return result;
	}
}
