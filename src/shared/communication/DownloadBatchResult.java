package shared.communication;

import java.util.ArrayList;

import shared.model.Field;
import shared.model.Batch;
import shared.model.Project;

/**
 * Container class to transport output for ClientCommunicator methods:
 * downloadBatch
 * @author bjs123
 */
public class DownloadBatchResult {

	private String status;
	private Project project;
	private ArrayList<Field> fields;
	private Batch batch;
	
	
	public DownloadBatchResult() {}

	public void succeed(Project project, ArrayList<Field> fields, Batch batch) {
		this.status = "TRUE";
		this.project = project;
		this.fields = fields;
		this.batch = batch;
		this.batch.setCheckedOut();
	}
	
	public void fail() {
		this.status = "FAILED";
		this.project = null;
		this.batch = null;
		this.fields = null;
	}
	
	public void prependFilePrefix(String prefix) {
		batch.setFile(prefix + batch.getFile());
		for(Field field: fields) {
			field.setHelpHTML(prefix + field.getHelpHTML());
			if(field.getKnownData().equals("null")) {
				field.setKnownData(null);
			}
			else {
				field.setKnownData(prefix + field.getKnownData());
			}
		}
	}
	
	public boolean getStatus() {
		if(this.status.equals("TRUE")) {
			return true;
		}
		else {
			return false;
		}
	}

	public Project getProject() {
		return project;
	}

	public ArrayList<Field> getFields() {
		return fields;
	}
	
	public Batch getBatch() {
		return batch;
	}
	
	public String toString() {
		String result = this.status + "\n";
		if(this.status.equals("TRUE")) {
			StringBuilder sb = new StringBuilder();
			sb.append(batch.getID() + "\n");
			sb.append(project.getID() + "\n");
			sb.append(batch.getFile() + "\n");
			sb.append(project.getFirstYCoord() + "\n");
			sb.append(project.getRecordHeight() + "\n");
			sb.append(project.getRecordsPerBatch() + "\n");
			sb.append(fields.size() + "\n");
			for(Field field: fields) {
				sb.append(fieldToString(field));
			}
			result = sb.toString();
		}
		return result;
	}
	
	private String fieldToString(Field field) {
		StringBuilder result = new StringBuilder();
		result.append(field.getID() + "\n");
		result.append(field.getPosition() + "\n");
		result.append(field.getTitle() + "\n");
		result.append(field.getHelpHTML() + "\n");
		result.append(field.getxCoord() + "\n");
		result.append(field.getWidth() + "\n");
		if(field.getKnownData() != null) {
			result.append(field.getKnownData() + "\n");
		}
		return result.toString();
	}

}
