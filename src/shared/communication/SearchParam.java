package shared.communication;

/**
 * Container class to transport input for ClientCommunicator methods:
 * search
 * @author bjs123
 */
public class SearchParam {

	private String username;
	private String password;
	private String fieldIDs;
	private String searchValues;
	
	/**
	 * Initializes private data.
	 * @param username
	 * @param password
	 * @param fieldIDs
	 * @param searchValues
	 */
	public SearchParam(String username, String password, String fieldIDs, String searchValues) {
		this.username = username;
		this.password = password;
		this.fieldIDs = fieldIDs;
		this.searchValues = searchValues;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFieldIDs() {
		return fieldIDs;
	}

	public String getSearchValues() {
		return searchValues;
	}
	
	public String toString() {
		return username + "\n" +
			   password + "\n" +
			   fieldIDs + "\n" + 
			   searchValues + "\n";
	}
}
