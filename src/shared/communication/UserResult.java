package shared.communication;

import shared.model.User;

/**
 * Container class to transport output for ClientCommunicator methods:
 * validateUser
 * @author bjs123
 */
public class UserResult {

	private String status;
	private User user;

	public UserResult() {}
	
	public void succeed(User user) {
		this.status = "TRUE";
		this.user = user;
	}

	public void fail() {
		this.status = "FAILED";
		this.user = null;
	}
	
	public void invalidate() {
		this.status = "FALSE";
		this.user = null;
	}

	public boolean getStatus() {
		if(status.equals("TRUE")) {
			return true;
		}
		return false;
	}

	public User getUser() {
		return user;
	}
	
	@Override
	public String toString() {
		String result = status + "\n";
		if(this.status.equals("TRUE")) {
			result += user.getFirstname() + "\n" +
					  user.getLastname() + "\n" + 
					  user.getIndexedRecords() + "\n";
		}
		return result;
	}
}
