package shared.communication;

/**
 * Container class to transport input for ClientCommunicator methods:
 * validateUser, getProjects
 * @author bjs123
 *
 */
public class UserParam {

	private String username;
	private String password;
	
	/**
	 * Initialize private data.
	 * @param username
	 * @param password
	 */
	public UserParam(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	public String toString() {
		return username + "\n" + password + "\n";
	}
}
