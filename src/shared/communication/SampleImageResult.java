package shared.communication;

/**
 * Container class to transport output for ClientCommunicator methods:
 * getSampleImage
 * @author bjs123
 */
public class SampleImageResult {

	private String status;
	private String file;
	
	public SampleImageResult() {}

	public void succeed() {
		this.status = "TRUE";
	}
	
	public void fail() {
		this.status = "FAILED";
		this.file = null;
	}
	
	public boolean getStatus() {
		if(this.status.equals("TRUE")) {
			return true;
		}
		else {
			return false;
		}
	}

	public String getFile() {
		return file;
	}

	public String toString() {
		String result = status + "\n";
		if(status.equals("TRUE")) {
			result = file;
		}
		return result;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
}