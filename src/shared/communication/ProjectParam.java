package shared.communication;

/**
 * Container class to transport input for ClientCommunicator methods:
 * getSampleImage, downloadBatch, getFields
 * @author bjs123
 */
public class ProjectParam {
	
	private String username;
	private String password;
	private int projectID;
	
	/**
	 * Initializes private data.
	 * @param username
	 * @param password
	 * @param projectID
	 */
	public ProjectParam(String username, String password, int projectID) {
		this.username = username;
		this.password = password;
		this.projectID = projectID;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getProjectID() {
		return projectID;
	}
	
	public String toString() {
		return username + "\n" + password + "\n" + projectID;
	}
}
