package shared.communication;

import java.util.ArrayList;
import java.util.Map;

import shared.model.Cell;

/**
 * Container class to transport output for ClientCommunicator methods:
 * search
 * @author bjs123
 */
public class SearchResult {

	private String status;
	private ArrayList<Cell> cells;
	private Map<Integer, String> images;
	
	public SearchResult() {}

	public void succeed(ArrayList<Cell> cells) {
		this.status = "TRUE";
		this.cells = cells;
	}
	
	public void setImages(Map<Integer, String> images) {
		this.images = images;
	}
	
	public void fail() {
		this.status = "FAILED";
		this.cells = null;
		this.images = null;
	}
	
	public String getStatus() {
		return this.status;
	}

	public ArrayList<Cell> getCells() {
		return cells;
	}

	public Map<Integer, String> getImages() {
		return this.images;
	}
	
	@Override
	public String toString() {
		String result = this.status + "\n";
		if(this.status.equals("TRUE")) {
			StringBuilder sb = new StringBuilder();
			for(Cell cell: cells) {
				sb.append(cell.getParentBatchID() + "\n");
				sb.append(images.get(cell.getParentBatchID()) + "\n");
				sb.append(cell.getRecordNumber() + "\n");
				sb.append(cell.getParentFieldID() + "\n");
			}
			result = sb.toString();
		}
		return result;
	}

}
