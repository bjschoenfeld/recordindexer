package shared.communication;

import java.util.ArrayList;

import shared.model.Project;

/**
 * Container class to transport output for ClientCommunicator methods:
 * getProjects
 * @author bjs123
 */
public class ProjectResult {

	private String status;
	private ArrayList<Project> projects;
	
	public ProjectResult() {}

	public void succeed(ArrayList<Project> projects) {
		status = "TRUE";
		this.projects = projects;
	}
	
	public void fail() {
		status = "FAILED";
		this.projects = null;
	}
	
	public boolean getStatus() {
		if(status.equals("TRUE")) {
			return true;
		}
		else {
			return false;
		}
	}

	public ArrayList<Project> getProjects() {
		return projects;
	}
	
	public String toString() {
		String result = status + "\n";
		if(status.equals("TRUE")) {
			StringBuilder sb = new StringBuilder();
			for(Project proj: projects) {
				sb.append(proj.getID() + "\n");
				sb.append(proj.getTitle() + "\n");
			}
			result = sb.toString();
		}
		return result;
	}
}
