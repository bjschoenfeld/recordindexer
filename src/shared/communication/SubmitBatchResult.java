package shared.communication;

public class SubmitBatchResult {

	private String status;
	
	public SubmitBatchResult() {}
	
	public void succeed() {
		this.status = "TRUE";
	}
	
	public void fail() {
		this.status = "FAILED";
	}
	
	public boolean getStatus() {
		return status.equals("TRUE");
	}
	
	@Override
	public String toString() {
		return this.status + "\n";
	}
}
