package shared.communication;

import java.util.ArrayList;

import shared.model.Field;

/**
 * Container class to transport output for ClientCommunicator methods:
 * getFields
 * @author bjs123
 */
public class FieldResult {

	private String status;
	private ArrayList<Field> fields;
	
	public FieldResult() {}

	public void succeed(ArrayList<Field> fields) {
		this.status = "TRUE";
		this.fields = fields;
	}
	
	public void fail() {
		this.status = "FAILED";
		this.fields = null;
	}
	
	public String getStatus() {
		return this.status;
	}

	public ArrayList<Field> getFields() {
		return fields;
	}
	
	public String toString() {
		String result = this.status + "\n";
		if(this.status.equals("TRUE")) {
			StringBuilder sb = new StringBuilder();
			for(Field field: fields) {
				sb.append(field.getParentProjectID() + "\n");
				sb.append(field.getID() + "\n");
				sb.append(field.getTitle() + "\n");
			}
			result = sb.toString();
		}
		return result;
	}
}
