package shared.communication;

/**
 * Container class to transport input for ClientCommunicator methods:
 * submitBatch
 * @author bjs123
 */
public class SubmitBatchParam {

	private String username;
	private String password;
	private int batchID;
	private String fieldValues;
	
	/**
	 * Initializes private data.
	 * @param username
	 * @param password
	 * @param batchID
	 * @param fieldValues
	 */
	public SubmitBatchParam(String username, String password, int batchID, String fieldValues) {
		this.username = username;
		this.password = password;
		this.batchID = batchID;
		this.fieldValues = fieldValues;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getBatchID() {
		return batchID;
	}

	public String getFieldValues() {
		return fieldValues;
	}
	
	public String toString() {
		return username + "\n" + 
			   password + "\n" +
			   batchID + "\n" +
			   fieldValues + "\n";
	}
}
