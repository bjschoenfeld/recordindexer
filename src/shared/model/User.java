package shared.model;

import importer.ApacheImporter;

import org.w3c.dom.Element;

public class User {
	
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private int indexedRecords;
	private int currentBatch;
	
	/**
	 * Used for creating a user from an xml file tree.
	 * @param userElement
	 */
	public User(Element userElement) {
		username = ApacheImporter.getValue((Element)userElement.getElementsByTagName("username").item(0));
		password = ApacheImporter.getValue((Element)userElement.getElementsByTagName("password").item(0));
		firstname = ApacheImporter.getValue((Element)userElement.getElementsByTagName("firstname").item(0));
		lastname = ApacheImporter.getValue((Element)userElement.getElementsByTagName("lastname").item(0));
		email = ApacheImporter.getValue((Element)userElement.getElementsByTagName("email").item(0));		
		indexedRecords = Integer.parseInt(ApacheImporter.getValue((Element)userElement.getElementsByTagName("indexedrecords").item(0)));
		currentBatch = 0;	
	}
	
	/**
	 * General purpose constructor.
	 * @param username
	 * @param password
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param indexedRecords
	 * @param currentBatch
	 */
	public User(String username, String password, String firstname,
			String lastname, String email, int indexedRecords, int currentBatch) {
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.indexedRecords = indexedRecords;
		this.currentBatch = currentBatch;
	}


	/**
	 * Formatted to insert user into sql table.
	 */
	public String toSQLString() {
		return "'" + username + "', '" + password + "', '" + firstname + "', '" + lastname + "', '" + email + "', " + indexedRecords + ", " + currentBatch;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getEmail() {
		return email;
	}

	public int getIndexedRecords() {
		return indexedRecords;
	}
	
	public int getCurrentBatch() {
		return currentBatch;
	}
}
