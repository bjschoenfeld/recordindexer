package shared.model;

import importer.ApacheImporter;

import org.w3c.dom.Element;

public class Cell {

	private int ID;
	private String data;
	private int parentFieldID;
	private int parentBatchID;
	private int recordNumber;

	public Cell(Element cellElement, int recordNumber) {
		data = ApacheImporter.getValue(cellElement);
		this.recordNumber = recordNumber;
	}

	public String toSQLString() {
		return "'" + data + "', " + parentFieldID + ", " + parentBatchID + ", " + recordNumber;
	}

	public Cell(int ID, String data,  int parentFieldID, int parentBatchID,	int recordNumber) {
		this.ID = ID;
		this.data = data;
		this.parentFieldID = parentFieldID;
		this.parentBatchID = parentBatchID;
		this.recordNumber = recordNumber;
	}

	public Cell(String data, int parentFieldID, int parentBatchID, int recordNumber) {
		this.data = data;
		this.parentFieldID = parentFieldID;
		this.parentBatchID = parentBatchID;
		this.recordNumber = recordNumber;
	}
	
	public void setParentFieldID(int fieldID) {
		this.parentFieldID = fieldID;		
	}
	
	public void setParentBatchID(int batchID) {
		this.parentBatchID = batchID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}

	public int getID() {
		return ID;
	}

	public String getData() {
		return data;
	}

	public int getParentBatchID() {
		return parentBatchID;
	}

	public int getParentFieldID() {
		return parentFieldID;
	}

	public int getRecordNumber() {
		return recordNumber;
	}

}
