package shared.model;

import importer.ApacheImporter;

import java.io.File;
import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Batch {

	private static final String directory = "Records" + File.separator;
	
	private int ID;
	private int parentProjectID;
	private String file;
	private ArrayList<Cell> cells;
	private String status;

	public Batch() {
		return;
	}
	
	public Batch(Element batchElement, ArrayList<Field> fields) {
		file = directory + ApacheImporter.getValue((Element)batchElement.getElementsByTagName("file").item(0));
		cells = new ArrayList<Cell>();
		status = "AVAILABLE";
		if(batchElement.getElementsByTagName("records").item(0) != null) {
			Element recordsElement = (Element)batchElement.getElementsByTagName("records").item(0);
			NodeList recordElements = recordsElement.getElementsByTagName("record");
			if(recordElements != null) {
				status = "SUBMITTED";
				for(int i = 0; i < recordElements.getLength(); i++) {
					Element record = (Element)recordElements.item(i);
					Element valuesElement = (Element)record.getElementsByTagName("values").item(0);
					NodeList valueElements = valuesElement.getElementsByTagName("value");
					for(int j = 0; j < valueElements.getLength(); j++) {
						Element value = (Element)valueElements.item(j);
						cells.add(new Cell(value, i+1));
					}
				}		
			}
		}
	}
	
	public String toSQLString() {
		return parentProjectID + ", '" + file + "', '" + status + "'";
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public int getParentProjectID() {
		return parentProjectID;
	}

	public void setParentProjectID(int parentProjectID) {
		this.parentProjectID = parentProjectID;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public ArrayList<Cell> getCells() {
		return cells;
	}

	public void setCells(ArrayList<Cell> cells) {
		this.cells = cells;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public void setAvailable() {
		this.status = "AVAILABLE";
	}
	
	public void setCheckedOut() {
		this.status = "CHECKEDOUT";
	}
	
	public void setSubmitted() {
		this.status = "SUBMITTED";
	}
	
}
