package shared.model;

import java.io.File;

import importer.ApacheImporter;

import org.w3c.dom.Element;

public class Field {
	
	private static final String directory = "Records" + File.separator;
	
	private int ID;
	private int parentProjectID;
	private int position;
	private String title;	
	private int xCoord;
	private int width;
	private String helpHTML;
	private String knownData;
	
	public Field(Element fieldElement, int position) {
		this.position = position;
		title = ApacheImporter.getValue((Element)fieldElement.getElementsByTagName("title").item(0));
		xCoord = Integer.parseInt(ApacheImporter.getValue((Element)fieldElement.getElementsByTagName("xcoord").item(0)));
		width = Integer.parseInt(ApacheImporter.getValue((Element)fieldElement.getElementsByTagName("width").item(0)));
		helpHTML = directory + ApacheImporter.getValue((Element)fieldElement.getElementsByTagName("helphtml").item(0));
		if(fieldElement.getElementsByTagName("knowndata").item(0) != null) {
			knownData = directory + ApacheImporter.getValue((Element)fieldElement.getElementsByTagName("knowndata").item(0));
		}
		else {
			knownData = null;
		}
	}
	
	public Field(int fieldID, int parentProjectID, int position, String title, int xCoord,
			int width, String helpHTML, String knownData) {
		this.ID = fieldID;
		this.parentProjectID = parentProjectID;
		this.position = position;
		this.title = title;
		this.xCoord = xCoord;
		this.width = width;
		this.helpHTML = helpHTML;
		this.knownData = knownData;
	}

	public void setID(int ID) {
		this.ID = ID;
	}
	
	public String toSQLString() {
		return parentProjectID + ", " + position + ", '"+ title + "', " + xCoord + ", " + width + 
				", '" + helpHTML + "', '" + knownData + "'";
	}
	
	public void setParentProjectID(int projectID) {
		this.parentProjectID = projectID;
	}

	public void setHelpHTML(String file) {
		this.helpHTML = file;
	}
	
	public void setKnownData(String file) {
		this.knownData = file;
	}
	
	public int getID() {
		return ID;
	}

	public int getParentProjectID() {
		return parentProjectID;
	}
	
	public int getPosition() {
		return position;
	}

	public String getTitle() {
		return title;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getWidth() {
		return width;
	}

	public String getHelpHTML() {
		return helpHTML;
	}

	public String getKnownData() {
		return knownData;
	}
}
