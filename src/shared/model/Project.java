package shared.model;

import importer.ApacheImporter;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Project {

	private int ID;
	private String title;
	private int recordsPerBatch;
	private int firstYCoord;
	private int recordHeight;
	private ArrayList<Field> fields;
	private ArrayList<Batch> batches;
	
	/**
	 * Creates a new Project object.
	 * @param projectElement An element object which contains all the necessary 
	 * data as sub elements.
	 */
	public Project(Element projectElement) {
		fields = new ArrayList<Field>();
		batches = new ArrayList<Batch>();
		title = ApacheImporter.getValue((Element)projectElement.getElementsByTagName("title").item(0));
		recordsPerBatch = Integer.parseInt(ApacheImporter.getValue((Element)projectElement.getElementsByTagName("recordsperimage").item(0)));
		firstYCoord = Integer.parseInt(ApacheImporter.getValue((Element)projectElement.getElementsByTagName("firstycoord").item(0)));
		recordHeight = Integer.parseInt(ApacheImporter.getValue((Element)projectElement.getElementsByTagName("recordheight").item(0)));
		Element fieldsElement = (Element)projectElement.getElementsByTagName("fields").item(0);
		NodeList fieldElements = fieldsElement.getElementsByTagName("field");
		for(int i = 0; i < fieldElements.getLength(); i++) {
			fields.add(new Field((Element)fieldElements.item(i), i+1));
		}
		Element batchesElement = (Element)projectElement.getElementsByTagName("images").item(0);
		NodeList batchElements = batchesElement.getElementsByTagName("image");
		for(int i = 0; i < batchElements.getLength(); i++) {
			batches.add(new Batch((Element)batchElements.item(i), fields));
		}
	}

	
	public Project(int iD, String title, int recordsPerBatch, int firstYCoord,
			int recordHeight) {
		ID = iD;
		this.title = title;
		this.recordsPerBatch = recordsPerBatch;
		this.firstYCoord = firstYCoord;
		this.recordHeight = recordHeight;
	}

	public void setID(int ID) {
		this.ID = ID;
	}
	/**
	 * Formatted for inserting into an sql table.
	 */
	public String toSQLString() {
		return "'" + title + "', " + recordsPerBatch + ", " + firstYCoord + ", " + recordHeight; 
	}

	public int getID() {
		return ID;
	}

	public String getTitle() {
		return title;
	}

	public int getRecordsPerBatch() {
		return recordsPerBatch;
	}

	public int getFirstYCoord() {
		return firstYCoord;
	}

	public int getRecordHeight() {
		return recordHeight;
	}

	public ArrayList<Field> getFields() {
		return fields;
	}

	public ArrayList<Batch> getBatches() {
		return batches;
	}
	
	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
	}
	
	public void setBatches(ArrayList<Batch> batches) {
		this.batches = batches;
	}
}
